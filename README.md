# AHRS
Attitude Heading Reference System

max msp library for calculating attitude and heading...
The AHRS Library (Attitude Heading Reference System) is a set of Max externals that allows you to perform a series of basic calculations for 3D/4D vectorial math used in aerodynamics. If you are using a three axis accelerometer and a three-axis magnetometer check out the"ahrs_triad" object which enables you to find the orientation of your cluster of sensor with respect to the Earth fixed coordinates.

1. Download the zip folder. 
2. Expand it 
3. Move it on to whtever MAX path of your choice

All code is available in the folder "Source Code"
If you are interested in contributing to the code all source code is in "/source" folder.
I used an old version of XCode...
If you are importing and compiling this library with more modern IDEs please let me know

