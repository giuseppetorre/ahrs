/*
   
 "quatnormalize" Object for Max/Msp 4.6
 AHRS MAX Library
 Copyright (C) 2013  Giuseppe Torre
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 
 */


#include "ext.h"    
#include "ext_mess.h"


void *this_class; // Required. Global pointing to this class  

 
typedef struct _quatnormalize // Data structure for this object  
{ 
	t_object m_ob;	// Must always be the first field; used by Max  
	Atom m_args[4];
	long m_value;	//inlet
	
	void *m_quat;


} t_quatnormalize; 
 

 
// Prototypes for methods: need a method for each incoming message 
void *quatnormalize_new(long value); 	// object creation method  
void quatnormalize_assist(t_quatnormalize *quatnormalize, void *b, long msg, long arg, char *s);
void quatnormalize_free(t_quatnormalize *quatnormalize);
void quatnormalize_list(t_quatnormalize *x, Symbol *s, short argc, t_atom *argv);


  
  

  
double q0, q1, q2, q3, magnitude;
double quatlist[4];
t_atom myList[4];



int main(void) 
{  
	// set up our class: create a class definition  
	setup((t_messlist**) &this_class, (method)quatnormalize_new, (method)quatnormalize_free, (short)sizeof(t_quatnormalize), 0L,A_GIMME, 0); 


    addmess((method)quatnormalize_list, "list", A_GIMME, 0);
	addmess((method)quatnormalize_assist, "assist", A_CANT, 0);
	finder_addclass("Maths","quatnormalize");
	
	


	post("ahrs_quatnormalize....loaded",0);

	return 0;
}


/*___________________------____________________________*/

/* ------------quatnormalize_new --------------*/

void *quatnormalize_new(long value) 
{ 
	t_quatnormalize *quatnormalize;

	quatnormalize = (t_quatnormalize *)newobject(this_class);	// create the new instance and return a pointer to it 
	
	quatnormalize->m_quat = listout(quatnormalize);

	return(quatnormalize);

} 





 
void quatnormalize_list(t_quatnormalize *x, Symbol *s, short argc, t_atom *argv)
{

q0 = argv[0].a_w.w_float;
q1 = argv[1].a_w.w_float;
q2 = argv[2].a_w.w_float;
q3 = argv[3].a_w.w_float;
short i;
						
						magnitude = 1.0 /  sqrt( pow(q0,2) + pow(q1,2) + pow(q2,2) + pow(q3,2));
				
						quatlist[0] = q0 * magnitude;
						quatlist[1] = q1 * magnitude;
						quatlist[2] = q2 * magnitude;
						quatlist[3] = q3 * magnitude;
		


			for (i = 0 ; i< 4; i++){

						SETFLOAT(myList+i, quatlist[i]);

			}


outlet_list( x-> m_quat , 0L, 4, myList);

	
}


/* -------- quatnormalize_assist ------------- */

void quatnormalize_assist(t_quatnormalize *quatnormalize, void *b, long msg, long arg, char *s)
{
	if (msg == ASSIST_OUTLET) {
		switch (arg) {
			case 0: sprintf(s, "%s", "List with Normalized Quaternion ");
				break;


		}
	}
	else if(msg == ASSIST_INLET)
		sprintf(s, "%s", "quaternion that needs to be n ormalized");
}
	

/* ----------- quatnormalize_free -------------*/

void quatnormalize_free(t_quatnormalize *quatnormalize)
{

		post("Fine!....BYE!", 0);

}



