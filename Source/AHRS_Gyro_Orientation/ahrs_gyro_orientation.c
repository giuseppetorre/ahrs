
 /*	
  
  "orientation" Object for Max/Msp 4.6																							*
  AHRS MAX Library
  Copyright (C) 2013  Giuseppe Torre
  
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  
  */


#include "ext.h"    
#include "ext_mess.h"
#include <string.h>
#include <stdio.h>
#include <math.h>
#define PI 3.1415926535897932384626433832795
#define EPSILON 1e-6




void *this_class;				// Required. Global pointing to this class  

 
typedef struct _orientation		// Data structure for this object  
{ 
	t_object m_ob;				// Must always be the first field; used by Max  
	
	Atom m_args[10];				// we want our inlet to be receiving a list of 10 elements
	
	long m_value;				//inlet
					
	void *m_R1;					//these is the outlet for the 3 X 3 Matrix
				// End Outlets ------------------------------------------------------------------
	
} t_orientation; 
 

			// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
			// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
			
																											// - //  Prototypes for methods: need a method for each incoming message   // - //
																												//									----------MAX FUNCTIONS
			void *orientation_new(long value);																	// object creation method  
			void orientation_assist(t_orientation *orientation, void *b, long msg, long arg, char *s);			// thing to display help message when you move the mouse over an in/outlet
			void orientation_free(t_orientation *orientation);													// free memory with the object instance
			
			void orientation_list(t_orientation *x, Symbol *s, short argc, t_atom *argv);						// what we going to do with our incoming list of values

																												//									----------OUR FUNCTIONS
			void InitiateMatrix(double *orientation,double *RotX,double *RotY, double *RotZ);					// Initiate the Matrix to Identity MAtrix  { 1 0 0    0 1 0   0 0 1 }
			void MatrixByMatrix(double *Result,double *MatrixLeft,double *MatrixRight);							// Multiply old and new matrix together
			void Matrix2Quat(double *Quat,double *Matrix);

			
			// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =

																											// - //
			double orientationMatrix[9];
			double RotationMatrixX[9];
			double RotationMatrixY[9];
			double RotationMatrixZ[9];
			double RotationMatrix[9];
			double Result[9];
			float  GyroAdcValues[3];
			float  GyroValuesRad[3];
			float  OldGyroValuesRad[3];
			double TrapeziumGyroResult[3];
			int    i;
			double  SAMPLING_RATE;
			double quat_old[4];
			double trace, Suca;
			double GYROX_OFFSET, GYROY_OFFSET, GYROZ_OFFSET, ALPHA_RATE_RESOLUTION, PHI_RATE_RESOLUTION,THETA_RATE_RESOLUTION;

			t_atom myList[9];


int main(void) 
{  
	// set up our class: create a class definition  
	setup((t_messlist**) &this_class, (method)orientation_new, (method)orientation_free, (short)sizeof(t_orientation), 0L,A_GIMME, 0); 


    addmess((method)orientation_list, "list", A_GIMME, 0);
	addmess((method)orientation_assist, "assist", A_CANT, 0);
	finder_addclass("Maths","orientation");
	

	
	InitiateMatrix(&orientationMatrix[0],&RotationMatrixX[0],&RotationMatrixY[0],&RotationMatrixZ[0]);



	post("...ahrs_gyro!...loaded",0);

	return 0;
}


/*___________________------____________________________*/

/* ------------orientation_new --------------*/

void *orientation_new(long value) 
{ 
	t_orientation *orientation;

	orientation = (t_orientation *)newobject(this_class);	// create the new instance and return a pointer to it 
	
    orientation->m_R1 = listout(orientation);


	return(orientation);

} 





 
void orientation_list(t_orientation *x, Symbol *s, short argc, t_atom *argv)
{


				GyroAdcValues[0]= argv[0].a_w.w_float; //roll angular speed gyro  adc------
				GyroAdcValues[1]= argv[1].a_w.w_float; //pitch angular speed gyro adc  ------
				GyroAdcValues[2]= argv[2].a_w.w_float; //yaw angular speed gyro   adc-------

				GYROX_OFFSET = argv[3].a_w.w_float;
				GYROY_OFFSET = argv[4].a_w.w_float;
				GYROZ_OFFSET = argv[5].a_w.w_float;

				ALPHA_RATE_RESOLUTION = argv[6].a_w.w_float;
				PHI_RATE_RESOLUTION   = argv[7].a_w.w_float;
				THETA_RATE_RESOLUTION = argv[8].a_w.w_float;

				SAMPLING_RATE = argv[9].a_w.w_float;  






//***********************************************************************************************************************************************//
//***********************************************************************************************************************************************//
//********************************************    U  S  E       G  Y  R  O  S  C  O  P  E  S   **************************************************//
//***********************************************************************************************************************************************//
//***********************************************************************************************************************************************//


      GyroValuesRad[0] = (GyroAdcValues[0] - GYROX_OFFSET) * (5.0/4096.0) * (PI/180.0) * (1.0/ALPHA_RATE_RESOLUTION);  // x 

      GyroValuesRad[1] = (GyroAdcValues[1] - GYROY_OFFSET) * (5.0/4096.0) * (PI/180.0) * (1.0/PHI_RATE_RESOLUTION);  // y

      GyroValuesRad[2] = (GyroAdcValues[2] - GYROZ_OFFSET) * (5.0/4096.0) * (PI/180.0) * (1.0/THETA_RATE_RESOLUTION);  // z




//************************ Integration using Trapezium Rule********************//

for (i=0;i<3;i++)

{

      TrapeziumGyroResult[i]=(OldGyroValuesRad[i]+GyroValuesRad[i]) * SAMPLING_RATE / 2.0;

      OldGyroValuesRad[i]=GyroValuesRad[i];

}
//******************************Obtaining new orientation matrix*******************************//
 
	RotationMatrixX[4] =  cos(TrapeziumGyroResult[0]);  
	RotationMatrixX[5] = -sin(TrapeziumGyroResult[0]);
	RotationMatrixX[7] =  sin(TrapeziumGyroResult[0]);
	RotationMatrixX[8] =  cos(TrapeziumGyroResult[0]);

	RotationMatrixY[0] =  cos(TrapeziumGyroResult[1]);
	RotationMatrixY[2] =  sin(TrapeziumGyroResult[1]);
	RotationMatrixY[6] = -sin(TrapeziumGyroResult[1]);
	RotationMatrixY[8] =  cos(TrapeziumGyroResult[1]);

	RotationMatrixZ[0] =  cos(TrapeziumGyroResult[2]);
	RotationMatrixZ[1] = -sin(TrapeziumGyroResult[2]);
	RotationMatrixZ[3] =  sin(TrapeziumGyroResult[2]);
	RotationMatrixZ[4] =  cos(TrapeziumGyroResult[2]);


	

	MatrixByMatrix(&Result[0],&RotationMatrixX[0],&RotationMatrixY[0]);
	MatrixByMatrix(&RotationMatrix[0],&Result[0],&RotationMatrixZ[0]);      
	MatrixByMatrix(&Result[0],&orientationMatrix[0],&RotationMatrix[0]);     
	

	
	for(i=0;i< 9 ;i++)orientationMatrix[i]= Result[i];//here is the new Orienation Matrix ready to use
	
//	Matrix2Quat( &quat_old[0], &Result[0]);
		


//Let's cook for output //

	for (i = 0 ; i< 9; i++)
	{
		SETFLOAT(myList+i, Result[i]);
	}


	outlet_list( x-> m_R1 , 0L, 9, myList);
	

}

//***********************************************************************************************************************************************//
//***********************************************************************************************************************************************//
//********************************************    E  N  D       G  Y  R  O  S  C  O  P  E  S   **************************************************//
//***********************************************************************************************************************************************//
//***********************************************************************************************************************************************//




//***********************************************************************************************************************************************//
//***********************************************************************************************************************************************//
//***********************************************************************************************************************************************//
//***********************************************************************************************************************************************//
//********************************************    A  L  L      F  U  N  C  T  I  O  N  S      ***************************************************//
//***********************************************************************************************************************************************//
//***********************************************************************************************************************************************//
//***********************************************************************************************************************************************//
//***********************************************************************************************************************************************//




  void InitiateMatrix(double *orientation,double *RotX,double *RotY,double *RotZ)
 {
	 for (i=0;i<9;i++)*(orientation+i)=0;


	 *orientation = 1.0;
	 *(orientation+4)=1.0;
	 *(orientation+8)=1.0;
	 *(RotZ+2)=0.0;
	 *(RotZ+5)=0.0;
	 *(RotZ+6)=0.0;
	 *(RotZ+7)=0.0;
	 *(RotZ+8)=1.0;
	 *(RotY+1)=0.0;
	 *(RotY+3)=0.0;
	 *(RotY+4)=1.0;
	 *(RotY+5)=0.0;
	 *(RotY+7)=0.0;
     *(RotX)=1.0; 
	 *(RotX+1)=0.0;
	 *(RotX+2)=0.0;
	 *(RotX+3)=0.0;
	 *(RotX+6)=0.0;       

 }
 





void MatrixByMatrix(double *Result,double *MatrixLeft,double *MatrixRight)
{
	
	*(Result)   = (  (*(MatrixLeft)) * (*(MatrixRight))    ) + (  (*(MatrixLeft+1)) * (*(MatrixRight+3))  ) + (  (*(MatrixLeft+2)) * (*(MatrixRight+6))  );
	*(Result+1) = (  (*(MatrixLeft)) * (*(MatrixRight+1))  ) + (  (*(MatrixLeft+1)) * (*(MatrixRight+4))  ) + (  (*(MatrixLeft+2)) * (*(MatrixRight+7))  );
	*(Result+2) = (  (*(MatrixLeft)) * (*(MatrixRight+2))  ) + (  (*(MatrixLeft+1)) * (*(MatrixRight+5))  ) + (  (*(MatrixLeft+2)) * (*(MatrixRight+8))  );
	
	*(Result+3) = (  (*(MatrixLeft+3)) * (*(MatrixRight))    ) + (  (*(MatrixLeft+4)) * (*(MatrixRight+3))  ) + (  (*(MatrixLeft+5)) * (*(MatrixRight+6))  );
	*(Result+4) = (  (*(MatrixLeft+3)) * (*(MatrixRight+1))  ) + (  (*(MatrixLeft+4)) * (*(MatrixRight+4))  ) + (  (*(MatrixLeft+5)) * (*(MatrixRight+7))  );
	*(Result+5) = (  (*(MatrixLeft+3)) * (*(MatrixRight+2))  ) + (  (*(MatrixLeft+4)) * (*(MatrixRight+5))  ) + (  (*(MatrixLeft+5)) * (*(MatrixRight+8))  );

	*(Result+6) = (  (*(MatrixLeft+6)) * (*(MatrixRight))    ) + (  (*(MatrixLeft+7)) * (*(MatrixRight+3))  ) + (  (*(MatrixLeft+8)) * (*(MatrixRight+6))  );
	*(Result+7) = (  (*(MatrixLeft+6)) * (*(MatrixRight+1))  ) + (  (*(MatrixLeft+7)) * (*(MatrixRight+4))  ) + (  (*(MatrixLeft+8)) * (*(MatrixRight+7))  );
	*(Result+8) = (  (*(MatrixLeft+6)) * (*(MatrixRight+2))  ) + (  (*(MatrixLeft+7)) * (*(MatrixRight+5))  ) + (  (*(MatrixLeft+8)) * (*(MatrixRight+8))  );
	
}



void Matrix2Quat(double *Quat,double *Matrix)
{


// this function convert a matrix 3 x 3 to a quaternion w x y z  



								trace = (*(Matrix)) + (*( Matrix + 4 ))  + (*( Matrix + 8 )) + 1.0;


								if( trace > EPSILON ){
      
										Suca = 0.5 / sqrt(trace);
										*(Quat) = 0.25 / Suca;											//w
										*(Quat + 1) = ( (*( Matrix + 7 )) - (*( Matrix + 5 )) ) * Suca; //x
										*(Quat + 2) = ( (*( Matrix + 2 )) - (*( Matrix + 6 )) ) * Suca; //y
										*(Quat + 3) = ( (*( Matrix + 3 )) - (*( Matrix + 1 )) ) * Suca; //z

	
								} else {

										if (    (      (*( Matrix )) > (*( Matrix + 4 ))   )    &&       (        (*( Matrix ))  > (*( Matrix + 8 ))     )     ){ 
				
												Suca = sqrt( 1.0 + (*( Matrix ))  - (*( Matrix + 4 )) - (*( Matrix + 8 )) ) * 2.0 ; // S=4*qx 
												*(Quat) = ( (*( Matrix + 7 )) - (*( Matrix + 5 )) ) / Suca;		// w	
												*(Quat + 1) = 0.25 * Suca;										// x
												*(Quat + 2) = ( (*( Matrix + 1 )) + (*( Matrix + 3 )) ) / Suca; // y 
												*(Quat + 3) = ( (*( Matrix + 2 )) + (*( Matrix + 6 )) ) / Suca; // z 
												
				
										} else if (  (*(Matrix + 4)) > (*(Matrix + 8)) ) { 
		
												Suca = sqrt( 1.0 + (*(Matrix + 4)) - (*(Matrix)) - (*(Matrix + 8)) ) * 2.0; // S=4*qy
												*(Quat) = ((*(Matrix + 2)) - (*(Matrix + 6))) / Suca;		// w
												*(Quat + 1) = ((*(Matrix + 1)) + (*(Matrix + 3)) ) / Suca;  // x
												*(Quat + 2) = 0.25 * Suca;									// y
												*(Quat + 3) = ((*(Matrix + 5)) + (*(Matrix + 7)) ) / Suca;  // z 
												
										} else { 
		
												Suca = sqrt( 1.0 + (*(Matrix + 8)) - (*(Matrix)) - (*(Matrix + 4)) ) * 2.0; // S=4*qz
												*(Quat)		= ((*(Matrix + 3)) - (*(Matrix + 1)) ) / Suca;  // w
												*(Quat + 1) = ((*(Matrix + 2)) + (*(Matrix + 6)) ) / Suca;  // x
												*(Quat + 2) = ((*(Matrix + 5)) + (*(Matrix + 7)) ) / Suca;  // y
												*(Quat + 3) = 0.25 * Suca;									// z
										
										} 

								}


		
							


}




/* -------- orientation_assist ------------- */

void orientation_assist(t_orientation *orientation, void *b, long msg, long arg, char *s)
{
	if (msg == ASSIST_OUTLET) {
		switch (arg) {
			case 0: sprintf(s, "%s", "matrix 3x3 (list)");
				break;
		}
	}
	else if(msg == ASSIST_INLET)
		sprintf(s, "%s", "pitch yaw roll GyroX_offset GyroY_offset GyroZ_offset ALPHA_rate_resolution PHI_rate_resolution THETA_rate_resolution Sampling_Rate(list)");
}
	

/* ----------- orientation_free -------------*/

void orientation_free(t_orientation *orientation)
{

		post("Fine!....BYE!", 0);

}

//***********************************************************************************************************************************************//
//***********************************************************************************************************************************************//
//***********************************************************************************************************************************************//
//***********************************************************************************************************************************************//
//***********************************************************************************************************************************************//
//***********************************************************************************************************************************************//
//***********************************************************************************************************************************************//
//***********************************************************************************************************************************************//
//***********************************************************************************************************************************************//

