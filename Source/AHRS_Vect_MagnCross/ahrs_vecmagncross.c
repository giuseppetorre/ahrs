/*
   
 "vecmagncross" Object for Max/Msp 4.6
 AHRS MAX Library
 Copyright (C) 2013  Giuseppe Torre
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 
 */


#include "ext.h"    
#include "ext_mess.h"
#define MAXSIZE 3

void *this_class; // Required. Global pointing to this class  

 
typedef struct _vecmagncross // Data structure for this object  
{ 
	t_object m_ob;	// Must always be the first field; used by Max  
	Atom m_args[3];
	void *m_proxy; /* 3 inlets requires 2 proxies */
	long m_inletNumber; /* where proxy will put inlet number */
	
	void *m_vec;


} t_vecmagncross; 
 

 
// Prototypes for methods: need a method for each incoming message 
void *vecmagncross_new(long value); 	// object creation method  
void vecmagncross_assist(t_vecmagncross *vecmagncross, void *b, long msg, long arg, char *s);
void vecmagncross_free(t_vecmagncross *vecmagncross);
void vecmagncross_list(t_vecmagncross *x, Symbol *s, short argc, t_atom *argv);
void vecmagncross_bang(t_vecmagncross *x);
  
  
double vecleft[3],  vecright[3];
double dotproduct, magncross, magn_v1, magn_v2;



int main(void) 
{  
	// set up our class: create a class definition  
	setup((t_messlist**) &this_class, (method)vecmagncross_new, (method)vecmagncross_free, (short)sizeof(t_vecmagncross), 0L,A_GIMME, 0); 

	addbang((method)vecmagncross_bang);
    addmess((method)vecmagncross_list, "list", A_GIMME, 0);
	addmess((method)vecmagncross_assist, "assist", A_CANT, 0);
	finder_addclass("Maths","vecmagncross");
	
	
	post("ahrs_vecmagncross....loaded",0);

	return 0;
}


/*___________________------____________________________*/

/* ------------vecmagncross_new --------------*/

void *vecmagncross_new(long value) 
{ 
	t_vecmagncross *vecmagncross;

	vecmagncross = (t_vecmagncross *)newobject(this_class);	// create the new instance and return a pointer to it 
	
	vecmagncross->m_vec = listout(vecmagncross);
	
	vecmagncross->m_proxy = proxy_new(vecmagncross,1,&vecmagncross->m_inletNumber);

	return(vecmagncross);

} 





 
void vecmagncross_list(t_vecmagncross *x, Symbol *s, short argc, t_atom *argv)
{

	long i = proxy_getinlet((t_object *)x);

	if (argc > MAXSIZE){ 
	argc = MAXSIZE;
	post ("vecmagncross ERROR sorry, I need three numbers only (x y z for each given vector).", 0);
	
	}
	else if (argc == MAXSIZE)
	{
			if (i == 0)
			{
		
					vecleft[0] =  argv[0].a_w.w_float;
					vecleft[1] =  argv[1].a_w.w_float;
					vecleft[2] =  argv[2].a_w.w_float;
			
			
			
					vecmagncross_bang(x);
			
			}else{
					vecright[0] =  argv[0].a_w.w_float;
					vecright[1] =  argv[1].a_w.w_float;
					vecright[2] =  argv[2].a_w.w_float;
				
								
			}

	
	}
	


	
}




void vecmagncross_bang(t_vecmagncross *x){




dotproduct = (vecleft[0] * vecright[0]) + (vecleft[1] * vecright[1]) + (vecleft[2] * vecright[2]);

magn_v1 = sqrt(  pow(vecleft[0],2)  +  pow(vecleft[1],2)  +  pow(vecleft[2],2)  );
magn_v2 = sqrt(  pow(vecright[0],2) +  pow(vecright[1],2) +  pow(vecright[2],2) );

magncross = magn_v1 * magn_v2 * sqrt( 1.0 - pow(dotproduct,2) );


outlet_float( x-> m_vec , magncross);



}




/* -------- vecmagncross_assist ------------- */

void vecmagncross_assist(t_vecmagncross *vecmagncross, void *b, long msg, long arg, char *s)
{
	if (msg == ASSIST_OUTLET) {
		switch (arg) {
			case 0: sprintf(s, "%s", "List with resulting vector");
				break;


		}
	}
	else if(msg == ASSIST_INLET)
				switch (arg) {
			case 0: sprintf(s, "First vector ");
				break;
			
			case 1: sprintf(s, "Second vector ");
				break;

		}
}
	

/* ----------- vecmagncross_free -------------*/

void vecmagncross_free(t_vecmagncross *vecmagncross)
{
		freeobject(vecmagncross -> m_proxy);
		
		post("Fine!....BYE!", 0);

}


