/*
   
 "quatadd" Object for Max/Msp 4.6
 AHRS MAX Library
 Copyright (C) 2013  Giuseppe Torre
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 
 */


#include "ext.h"    
#include "ext_mess.h"
#define MAXSIZE 4

void *this_class; // Required. Global pointing to this class  

 
typedef struct _quatadd // Data structure for this object  
{ 
	t_object m_ob;	// Must always be the first field; used by Max  
	Atom m_args[4];
	void *m_proxy; /* 3 inlets requires 2 proxies */
	long m_inletNumber; /* where proxy will put inlet number */
	
	void *m_quat;


} t_quatadd; 
 

 
// Prototypes for methods: need a method for each incoming message 
void *quatadd_new(long value); 	// object creation method  
void quatadd_assist(t_quatadd *quatadd, void *b, long msg, long arg, char *s);
void quatadd_free(t_quatadd *quatadd);
void quatadd_list(t_quatadd *x, Symbol *s, short argc, t_atom *argv);
void quatadd_bang(t_quatadd *x);
  
  
double quatleft[4], quatout[4];
double quatright[4];



int main(void) 
{  
	// set up our class: create a class definition  
	setup((t_messlist**) &this_class, (method)quatadd_new, (method)quatadd_free, (short)sizeof(t_quatadd), 0L,A_GIMME, 0); 

	addbang((method)quatadd_bang);
    addmess((method)quatadd_list, "list", A_GIMME, 0);
	addmess((method)quatadd_assist, "assist", A_CANT, 0);
	finder_addclass("Maths","quatadd");
	
	
	post("ahrs_quatadd....loaded",0);

	return 0;
}


/*___________________------____________________________*/

/* ------------quatadd_new --------------*/

void *quatadd_new(long value) 
{ 
	t_quatadd *quatadd;

	quatadd = (t_quatadd *)newobject(this_class);	// create the new instance and return a pointer to it 
	
	quatadd->m_quat = listout(quatadd);
	
	quatadd->m_proxy = proxy_new(quatadd,1,&quatadd->m_inletNumber);

	return(quatadd);

} 





 
void quatadd_list(t_quatadd *x, Symbol *s, short argc, t_atom *argv)
{

	long i = proxy_getinlet((t_object *)x);

	if (argc > MAXSIZE){ 
	argc = MAXSIZE;
	post ("quatadd ERROR sorry, I need four numbers only (q0 q1 q2 q3 for each given quaternion).", 0);
	
	}
	else if (argc == MAXSIZE)
	{
			if (i == 0)
			{
		
					quatleft[0] =  argv[0].a_w.w_float;
					quatleft[1] =  argv[1].a_w.w_float;
					quatleft[2] =  argv[2].a_w.w_float;
					quatleft[3] =  argv[3].a_w.w_float;
			
			
					quatadd_bang(x);
			
			}else{
					quatright[0] =  argv[0].a_w.w_float;
					quatright[1] =  argv[1].a_w.w_float;
					quatright[2] =  argv[2].a_w.w_float;
					quatright[3] =  argv[3].a_w.w_float;
								
			}

	
	}
	


	
}




void quatadd_bang(t_quatadd *x){


t_atom myList[3];
	
						quatout[0] = quatleft[0] + quatright[0];
						quatout[1] = quatleft[1] + quatright[1];
						quatout[2] = quatleft[2] + quatright[2];
						quatout[3] = quatleft[3] + quatright[3];
					

	
short i;	
	
			for (i = 0 ; i< 4; i++){

						SETFLOAT(myList+i, quatout[i]);

			}


	outlet_list( x-> m_quat , 0L, 4, myList);



}




/* -------- quatadd_assist ------------- */

void quatadd_assist(t_quatadd *quatadd, void *b, long msg, long arg, char *s)
{
	if (msg == ASSIST_OUTLET) {
		switch (arg) {
			case 0: sprintf(s, "%s", "List with resulting quaternion");
				break;


		}
	}
	else if(msg == ASSIST_INLET)
				switch (arg) {
			case 0: sprintf(s, "First quaternion ");
				break;
			
			case 1: sprintf(s, "Second quaternion ");
				break;

		}
}
	

/* ----------- quatadd_free -------------*/

void quatadd_free(t_quatadd *quatadd)
{
		freeobject(quatadd -> m_proxy);
		
		post("Fine!....BYE!", 0);

}


