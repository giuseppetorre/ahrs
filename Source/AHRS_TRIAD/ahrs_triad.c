
 /*	
  
 "triad" Object for Max/Msp 4.6																									
  AHRS MAX Library
  Copyright (C) 2013  Giuseppe Torre
  
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  
  */


#include "ext.h"    
#include "ext_mess.h"
#include <string.h>
#include <stdio.h>
#include <math.h>
#define EPSILON 1e-6


//******************CALIBRATION STUFF*************************//




void *this_class;				// Required. Global pointing to this class  

 
typedef struct _triad		// Data structure for this object  
{ 
	t_object m_ob;				// Must always be the first field; used by Max  
	
	Atom m_args[12];				// we want our inlet to be receiving a list of 10 elements
	
	long m_value;				//inlet
					
	void *m_R1;					//these are all the outlets for the 3 X 3 Matrix
	void *m_R2;					// R1 --> firts top left cell ....R2 middle cell of the first row ...and so on
	void *m_R3;					//
	void *m_R4;					// End Outlets ------------------------------------------------------------------
	
} t_triad; 
 

			// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
			// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
			
																											// - //  Prototypes for methods: need a method for each incoming message   // - //
																												//									----------MAX FUNCTIONS
			void *triad_new(long value);																	// object creation method  
			void triad_assist(t_triad *triad, void *b, long msg, long arg, char *s);			// thing to display help message when you move the mouse over an in/outlet
			void triad_free(t_triad *triad);													// free memory with the object instance
			
			void triad_list(t_triad *x, Symbol *s, short argc, t_atom *argv);						// what we going to do with our incoming list of values


																												//									----------OUR FUNCTIONS
			void MatrixByMatrix(double *Result,double *MatrixLeft,double *MatrixRight);							// Multiply old and new matrix together
			void Matrix2Quat(double *Quat,double *Matrix);
			void Quat2Matrix(double *Matrix, double *Quat); 
			void inverseQuat(double *InvQuat, double *RegQuat); 
			void NormQuat(double *YesQuat, double *NotQuat);
			void Slerp(double *NewQuat, double *OldQuat, double *CurrentQuat);
			void NormVect(double *YesVect, double *NotVect);
			// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =

																											// - //
			double orientationMatrix[9];
			double Result[9];
			int    i;
			double InvorientationMatrix[9];						
			double temp[6];
			double ref[6];
			double vectAx[3];
			double vectAy[3];
			double vectAz[3];
			double vectBx[3];
			double vectBy[3];
			double vectBz[3];
			double MagnCrosProd_A;
			double MagnCrosProd_B;
			double accnorm, magnorm, earthnorm, gravnorm, VectAynorm, VectAznorm, VectBynorm, VectBznorm;
			double m[9], n[9];
			double quat_e[4];
			double invquat_e[4];
			double mult;
			double quat_new[4];
			double quat_old[4];
			double ecs,accex_ADCnumber,y,accey_ADCnumber,z,accez_ADCnumber,mx,magnx_ADCnumber,my,magny_ADCnumber,mz,magnz_ADCnumber;

			
// SLERP Variables
			double trace, Suca;
			double tol[4], omega, sinom, cosom, scale0, scale1, tez, orientationMatrixA[9];
			
					
							
									
													
int main(void) 
{  
	// set up our class: create a class definition  
	setup((t_messlist**) &this_class, (method)triad_new, (method)triad_free, (short)sizeof(t_triad), 0L,A_GIMME, 0); 


    addmess((method)triad_list, "list", A_GIMME, 0);
	addmess((method)triad_assist, "assist", A_CANT, 0);
	finder_addclass("Maths","triad");
	
	


	post("....I'm TRIAD Object!....from AHRS Library...",0);

	return 0;
}


/*___________________------____________________________*/

/* ------------triad_new --------------*/

void *triad_new(long value) 
{ 
	t_triad *triad;

	triad = (t_triad *)newobject(this_class);	// create the new instance and return a pointer to it 
	
	

	triad->m_R4 = floatout(triad);
	triad->m_R3 = floatout(triad);
	triad->m_R2 = floatout(triad);
	triad->m_R1 = floatout(triad);



	return(triad);

} 





 
void triad_list(t_triad *x, Symbol *s, short argc, t_atom *argv)
{

//***********************************************************************************************************************************************//
//***********************************************************************************************************************************************//
//***************************************   T	R	I	A	D		A	L	G	O	R	I	T	H	M	*****************************************//
//***********************************************************************************************************************************************//
//***********************************************************************************************************************************************//


//////////////////////////////////////////////////////////////    Creating the two vectors from the two sensors 
////////////////////////////////////////////////////////////// 

// Vector ONE is the stongest ---  Accelerometers or other

			temp[0] = argv[0].a_w.w_float;
			temp[1] = argv[1].a_w.w_float;
			temp[2] = argv[2].a_w.w_float;
			
//Normalizing Vector Acclerometer

			accnorm = 1.0 / sqrt( temp[0] * temp[0] + temp[1] * temp[1] + temp[2] * temp[2] ); 
			
			temp[0] *= accnorm;
			temp[1] *= accnorm;
			temp[2] *= accnorm;
    

// Vector TWO is the weakest ---- Magnetometers or other		                


			temp[3] = argv[3].a_w.w_float;
			temp[4] = argv[4].a_w.w_float;
			temp[5] = argv[5].a_w.w_float; 


// Normalizing Vector Magnetometer
				
			magnorm = 1.0 / sqrt( temp[3] * temp[3] + temp[4] * temp[4] + temp[5] * temp[5] );
	
			temp[3] *= magnorm;
			temp[4] *= magnorm;
			temp[5] *= magnorm;


////////////////////////////////////////////////////////////// 
//////////////////////////////////////////////////////////////    Creating the two vectors from the two Reference System


// ---  Gravity

			ref[0] = argv[6].a_w.w_float;
			ref[1] = argv[7].a_w.w_float;
			ref[2] = argv[8].a_w.w_float;
 
	gravnorm = 1.0 / sqrt( ref[0] * ref[0] + ref[1] * ref[1] + ref[2] * ref[2] );
	
			ref[0] *= gravnorm;
			ref[1] *= gravnorm;
			ref[2] *= gravnorm; 

// ---- Magnetic Earth Field		                
// check this page http://www.ngdc.noaa.gov/geomagmodels/IGRFWMM.jsp 
//======= use scale objecy in max/msp to determine values (scale 90 - 90 1 -1)  then y and z same but -90 degrees with respect to x

			ref[3] = argv[9].a_w.w_float;
			ref[4] = argv[10].a_w.w_float;
			ref[5] = argv[11].a_w.w_float; 
	
	earthnorm = 1.0 / sqrt( ref[3] * ref[3] + ref[4] * ref[4] + ref[5] * ref[5] );
	
			ref[3] *= earthnorm;
			ref[4] *= earthnorm;
			ref[5] *= earthnorm;

//////////////////////////////////////////////////////////////    
//////////////////     TRIAD ALGORITHM     ///////////////////
//////////////////////////////////////////////////////////////

////////////////////////////////////////////////  Body Vectors
// VectAx
			vectAx[0] = temp[0];
			vectAx[1] = temp[1];
			vectAx[2] = temp[2];

// VectAy
			
			MagnCrosProd_A = 1.0/ sqrt (1.0 -  (   ((temp[0] * temp[3]) + (temp[1] * temp[4])  + (temp[2] * temp[5]))  * ((temp[0] * temp[3]) + (temp[1] * temp[4])  + (temp[2] * temp[5]))   )  );
			
			vectAy[0] = (( temp[1] * temp[5] ) - ( temp[2] * temp[4] )) * MagnCrosProd_A;
			vectAy[1] = (( temp[2] * temp[3] ) - ( temp[0] * temp[5] )) * MagnCrosProd_A;
			vectAy[2] = (( temp[0] * temp[4] ) - ( temp[1] * temp[3] )) * MagnCrosProd_A;
 
			NormVect(&vectAy[0], &vectAy[0]);
				         	  

// VectAz 	

			vectAz[0] = ( vectAx[1] * vectAy[2] ) - ( vectAx[2] * vectAy[1] ) ;
			vectAz[1] = ( vectAx[2] * vectAy[0] ) - ( vectAx[0] * vectAy[2] ) ;
			vectAz[2] = ( vectAx[0] * vectAy[1] ) - ( vectAx[1] * vectAy[0] ) ;

			NormVect(&vectAz[0], &vectAz[0]);
	

////////////////////////////////////////////////

//////////////////////////////////////////////////////  Reference System Vectors

// VectBx
			vectBx[0] = ref[0];
			vectBx[1] = ref[1];
			vectBx[2] = ref[2];

// VectBy

			MagnCrosProd_B = 1.0/ sqrt (1.0 -  (   ((ref[0] * ref[3]) + (ref[1] * ref[4])  + (ref[2] * ref[5]))  * ((ref[0] * ref[3]) + (ref[1] * ref[4])  + (ref[2] * ref[5]))   )  );

			vectBy[0] = (( ref[1] * ref[5] ) - ( ref[2] * ref[4] )) * MagnCrosProd_B;
			vectBy[1] = (( ref[2] * ref[3] ) - ( ref[0] * ref[5] )) * MagnCrosProd_B;
			vectBy[2] = (( ref[0] * ref[4] ) - ( ref[1] * ref[3] )) * MagnCrosProd_B;
			
			NormVect(&vectBy[0], &vectBy[0]);
		

// VectBz 	

			vectBz[0] = ( vectBx[1] * vectBy[2] ) - ( vectBx[2] * vectBy[1] ) ;
			vectBz[1] = ( vectBx[2] * vectBy[0] ) - ( vectBx[0] * vectBy[2] ) ;
			vectBz[2] = ( vectBx[0] * vectBy[1] ) - ( vectBx[1] * vectBy[0] ) ;
		
			NormVect(&vectBz[0], &vectBz[0]);	

//////////////////////////////////////////////////////

//////////////////////////////////////////////////////    Multiply vectors and Generate Rotation Matrix


			m[0] = vectAx[0];	m[1] = vectAy[0];		m[2] = vectAz[0];    //columns has become rows
			m[3] = vectAx[1];	m[4] = vectAy[1];		m[5] = vectAz[1];
			m[6] = vectAx[2];	m[7] = vectAy[2];		m[8] = vectAz[2];


			n[0] = vectBx[0];	n[1] = vectBx[1];		n[2] = vectBx[2];    // as it was
			n[3] = vectBy[0];	n[4] = vectBy[1];		n[5] = vectBy[2];
			n[6] = vectBz[0];	n[7] = vectBz[1];		n[8] = vectBz[2];



// We generate our Matrix 3x3 from our 6 vectors the we apply the following transpormation (in order):
// Conversion Matrix to Quaternion 
// Normalize Quaternion
// Invert Quaternion  


			MatrixByMatrix(&orientationMatrix[0],&m[0],&n[0]);	
	
			Matrix2Quat( &quat_e[0], &orientationMatrix[0]);	
	
			NormQuat( &quat_e[0], &quat_e[0]);

			inverseQuat(&quat_e[0], &quat_e[0]);
	
			Slerp(&quat_new[0], &quat_old[0], &quat_e[0]);

			NormQuat( &quat_new[0], &quat_new[0]);

			
			Quat2Matrix(&orientationMatrix[0], &quat_new[0]);	







/// COOK THE  OUTPUT !!!!!!!
	
						for(i=0; i<9 ;i++) Result[i]= orientationMatrix[i];
						for(i=0; i<4 ;i++) quat_old[i]= quat_new[i];
						


/// END     COOK THE OUTPUT !!!!!!!


	
	

		
	
	outlet_float((x -> m_R1), quat_new[0] );
	outlet_float((x -> m_R2), quat_new[1] );
	outlet_float((x -> m_R3), quat_new[2] );
	outlet_float((x -> m_R4), quat_new[3] );



//***********************************************************************************************************************************************//
//***********************************************************************************************************************************************//
//*******************    E  N  D      A  C  C  E  L  E  R  O  M  E  T  E  R  +  M  A  G  N  E  T  O  M  E  T  E  R    ***************************//
//***********************************************************************************************************************************************//
//***********************************************************************************************************************************************//




}



//***********************************************************************************************************************************************//
//***********************************************************************************************************************************************//
//***********************************************************************************************************************************************//
//***********************************************************************************************************************************************//
//********************************************    A  L  L      F  U  N  C  T  I  O  N  S      ***************************************************//
//***********************************************************************************************************************************************//
//***********************************************************************************************************************************************//
//***********************************************************************************************************************************************//
//***********************************************************************************************************************************************//









void MatrixByMatrix(double *Result,double *MatrixLeft,double *MatrixRight)
{
	
	*(Result)   = (  (*(MatrixLeft)) * (*(MatrixRight))    ) + (  (*(MatrixLeft+1)) * (*(MatrixRight+3))  ) + (  (*(MatrixLeft+2)) * (*(MatrixRight+6))  );
	*(Result+1) = (  (*(MatrixLeft)) * (*(MatrixRight+1))  ) + (  (*(MatrixLeft+1)) * (*(MatrixRight+4))  ) + (  (*(MatrixLeft+2)) * (*(MatrixRight+7))  );
	*(Result+2) = (  (*(MatrixLeft)) * (*(MatrixRight+2))  ) + (  (*(MatrixLeft+1)) * (*(MatrixRight+5))  ) + (  (*(MatrixLeft+2)) * (*(MatrixRight+8))  );
	
	*(Result+3) = (  (*(MatrixLeft+3)) * (*(MatrixRight))    ) + (  (*(MatrixLeft+4)) * (*(MatrixRight+3))  ) + (  (*(MatrixLeft+5)) * (*(MatrixRight+6))  );
	*(Result+4) = (  (*(MatrixLeft+3)) * (*(MatrixRight+1))  ) + (  (*(MatrixLeft+4)) * (*(MatrixRight+4))  ) + (  (*(MatrixLeft+5)) * (*(MatrixRight+7))  );
	*(Result+5) = (  (*(MatrixLeft+3)) * (*(MatrixRight+2))  ) + (  (*(MatrixLeft+4)) * (*(MatrixRight+5))  ) + (  (*(MatrixLeft+5)) * (*(MatrixRight+8))  );

	*(Result+6) = (  (*(MatrixLeft+6)) * (*(MatrixRight))    ) + (  (*(MatrixLeft+7)) * (*(MatrixRight+3))  ) + (  (*(MatrixLeft+8)) * (*(MatrixRight+6))  );
	*(Result+7) = (  (*(MatrixLeft+6)) * (*(MatrixRight+1))  ) + (  (*(MatrixLeft+7)) * (*(MatrixRight+4))  ) + (  (*(MatrixLeft+8)) * (*(MatrixRight+7))  );
	*(Result+8) = (  (*(MatrixLeft+6)) * (*(MatrixRight+2))  ) + (  (*(MatrixLeft+7)) * (*(MatrixRight+5))  ) + (  (*(MatrixLeft+8)) * (*(MatrixRight+8))  );
	
}



void Matrix2Quat(double *Quat,double *Matrix)
{


// this function convert a matrix 3 x 3 to a quaternion w x y z  



								trace = (*(Matrix)) + (*( Matrix + 4 ))  + (*( Matrix + 8 )) + 1.0;


								if( trace > EPSILON ){
      
										Suca = 0.5 / sqrt(trace);
										*(Quat) = 0.25 / Suca;											//w
										*(Quat + 1) = ( (*( Matrix + 7 )) - (*( Matrix + 5 )) ) * Suca; //x
										*(Quat + 2) = ( (*( Matrix + 2 )) - (*( Matrix + 6 )) ) * Suca; //y
										*(Quat + 3) = ( (*( Matrix + 3 )) - (*( Matrix + 1 )) ) * Suca; //z

	
								} else {

										if (    (      (*( Matrix )) > (*( Matrix + 4 ))   )    &&       (        (*( Matrix ))  > (*( Matrix + 8 ))     )     ){ 
				
												Suca = sqrt( 1.0 + (*( Matrix ))  - (*( Matrix + 4 )) - (*( Matrix + 8 )) ) * 2.0 ; // S=4*qx 
												*(Quat) = ( (*( Matrix + 7 )) - (*( Matrix + 5 )) ) / Suca;		// w	
												*(Quat + 1) = 0.25 * Suca;										// x
												*(Quat + 2) = ( (*( Matrix + 1 )) + (*( Matrix + 3 )) ) / Suca; // y 
												*(Quat + 3) = ( (*( Matrix + 2 )) + (*( Matrix + 6 )) ) / Suca; // z 
												
				
										} else if (  (*(Matrix + 4)) > (*(Matrix + 8)) ) { 
		
												Suca = sqrt( 1.0 + (*(Matrix + 4)) - (*(Matrix)) - (*(Matrix + 8)) ) * 2.0; // S=4*qy
												*(Quat) = ((*(Matrix + 2)) - (*(Matrix + 6))) / Suca;		// w
												*(Quat + 1) = ((*(Matrix + 1)) + (*(Matrix + 3)) ) / Suca;  // x
												*(Quat + 2) = 0.25 * Suca;									// y
												*(Quat + 3) = ((*(Matrix + 5)) + (*(Matrix + 7)) ) / Suca;  // z 
												
										} else { 
		
												Suca = sqrt( 1.0 + (*(Matrix + 8)) - (*(Matrix)) - (*(Matrix + 4)) ) * 2.0; // S=4*qz
												*(Quat)		= ((*(Matrix + 3)) - (*(Matrix + 1)) ) / Suca;  // w
												*(Quat + 1) = ((*(Matrix + 2)) + (*(Matrix + 6)) ) / Suca;  // x
												*(Quat + 2) = ((*(Matrix + 5)) + (*(Matrix + 7)) ) / Suca;  // y
												*(Quat + 3) = 0.25 * Suca;									// z
										
										} 

								}


		
							


}





void inverseQuat(double *InvQuat, double *RegQuat)
{

						*(InvQuat) = *(RegQuat);
						*(InvQuat + 1) = -1.0 * (*(RegQuat + 1));
						*(InvQuat + 2) = -1.0 * (*(RegQuat + 2));
						*(InvQuat + 3) = -1.0 * (*(RegQuat + 3));

}








void NormQuat(double *YesQuat, double *NotQuat)
{


						mult = 1.0 /  sqrt( pow((*(NotQuat)),2) + pow((*(NotQuat + 1)),2) + pow((*(NotQuat + 2)),2) + pow((*(NotQuat + 3)),2));

						(*(YesQuat)) = (*(NotQuat)) * mult;
						(*(YesQuat + 1)) = (*(NotQuat + 1)) * mult;
						(*(YesQuat + 2)) = (*(NotQuat + 2)) * mult;
						(*(YesQuat + 3)) = (*(NotQuat + 3)) * mult;


}







void Slerp(double *NewQuat, double *OldQuat, double *CurrentQuat)
{


// SLERP code taken from http://www.gamasutra.com/features/19980703/quaternions_01.htm 



						cosom = (*(OldQuat + 1)) * (*(CurrentQuat + 1)) + (*(OldQuat + 2)) * (*(CurrentQuat + 2)) + (*(OldQuat + 3)) * (*(CurrentQuat + 3)) + (*(OldQuat)) * (*(CurrentQuat)) ;
						//post (" cosom %4.8f", cosom);
						tez = 0.8;
					
										if (cosom < 0.0 ){ 
		
													cosom = -1.0 * cosom ; 
													tol[0] = -1.0 * (*(CurrentQuat));
													tol[1] = -1.0 * (*(CurrentQuat + 1));
													tol[2] = -1.0 * (*(CurrentQuat + 2));
													tol[3] = -1.0 * (*(CurrentQuat + 3));
		
										}else { 
		
													tol[0] = (*(CurrentQuat));
													tol[1] = (*(CurrentQuat + 1));
													tol[2] = (*(CurrentQuat + 2));
													tol[3] = (*(CurrentQuat + 3));
						
										}

										
										if ( (1.0 - cosom) > EPSILON ){
		
													omega = acos(cosom);
													sinom = sin (omega);
													scale0 = sin ((1.0 - tez) * omega) / sinom;
													scale1 = sin (tez * omega) / sinom;
			
													//post("SPHERICAL       LINEARRRRRRRRRRRRR", 0);
		
										}else{
		
													scale0 = 1.0 - tez;
													scale1 = tez;

										}	


		
										(*(NewQuat)) = scale0 * (*(OldQuat)) + scale1 * tol[0];
										(*(NewQuat + 1)) = scale0 * (*(OldQuat + 1)) + scale1 * tol[1];
										(*(NewQuat + 2)) = scale0 * (*(OldQuat + 2)) + scale1 * tol[2];
										(*(NewQuat + 3)) = scale0 * (*(OldQuat + 3)) + scale1 * tol[3];



}







void Quat2Matrix(double *Matrix, double *Quat)
{


						(*( Matrix ))	  = 1.0 - (2.0 * pow((*(Quat + 2)), 2.0) ) - (2.0  *   pow( (*(Quat + 3)) , 2.0 ) );  
						(*( Matrix + 1 )) =		 (2.0 * (*(Quat + 1)) * (*(Quat + 2)) ) - (2.0 * (*(Quat + 3)) * (*(Quat)) );
						(*( Matrix + 2 )) =		 (2.0 * (*(Quat + 1)) * (*(Quat + 3)) ) + (2.0 * (*(Quat + 2)) * (*(Quat)) );
	 
						(*( Matrix + 3 )) = (2.0 * (*(Quat + 1)) * (*(Quat + 2))) + (2.0 * (*(Quat + 3))  * (*(Quat)));
						(*( Matrix + 4 )) = 1.0 - ( 2.0 * pow ( (*(Quat + 1)), 2.0 ) ) - ( 2.0 * pow ( (*(Quat + 3)) , 2 ) );
						(*( Matrix + 5 )) = (2.0 * (*(Quat + 2)) * (*(Quat + 3)) ) - (2.0 * (*(Quat + 1)) * (*(Quat)));
	 
						(*( Matrix + 6 )) = (2.0 * (*(Quat + 1)) * (*(Quat + 3)) ) - (2.0 * (*(Quat + 2)) * (*(Quat)));
						(*( Matrix + 7 )) = (2.0 * (*(Quat + 2)) * (*(Quat + 3))) + (2.0 * (*(Quat + 1)) * (*(Quat)));
						(*( Matrix + 8 )) = 1.0 - ( 2.0 * pow ( (*(Quat + 1)), 2.0 ) ) - ( 2.0 * pow ( (*(Quat + 2)), 2 ) );


}





void NormVect(double *YesVect, double *NotVect)
{


 
		VectBznorm = 1 / sqrt( (*(NotVect)) * (*(NotVect)) + (*(NotVect + 1)) * (*(NotVect + 1)) + (*(NotVect + 2)) * (*(NotVect + 2)) );
	
			(*(YesVect))	 *= VectBznorm;
			(*(YesVect + 1)) *= VectBznorm;
			(*(YesVect + 2)) *= VectBznorm;


}






/* -------- triad_assist ------------- */

void triad_assist(t_triad *triad, void *b, long msg, long arg, char *s)
{
	if (msg == ASSIST_OUTLET) {
		switch (arg) {
			case 0: sprintf(s, "%s", "qw ");
				break;
			case 1: sprintf(s, "%s", "qx");
				break;
			case 2: sprintf(s, "%s", "qy");
				break;
			case 3: sprintf(s, "%s", "qz");
				break;

		}
	}
	else if(msg == ASSIST_INLET)
		sprintf(s, "%s", "(list)");
}
	

/* ----------- triad_free -------------*/

void triad_free(t_triad *triad)
{

		post("Fine!....BYE!", 0);

}

//***********************************************************************************************************************************************//
//***********************************************************************************************************************************************//
//***********************************************************************************************************************************************//
//***********************************************************************************************************************************************//
//***********************************************************************************************************************************************//
//***********************************************************************************************************************************************//
//***********************************************************************************************************************************************//
//***********************************************************************************************************************************************//
//***********************************************************************************************************************************************//

