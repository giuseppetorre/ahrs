/*
   
 "quatdiv" Object for Max/Msp 4.6
 AHRS MAX Library
 Copyright (C) 2013  Giuseppe Torre
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 
 */


#include "ext.h"    
#include "ext_mess.h"
#define MAXSIZE 4

void *this_class; // Required. Global pointing to this class  

 
typedef struct _quatdiv // Data structure for this object  
{ 
	t_object m_ob;	// Must always be the first field; used by Max  
	Atom m_args[4];
	void *m_proxy; /* 3 inlets requires 2 proxies */
	long m_inletNumber; /* where proxy will put inlet number */
	
	void *m_quat;


} t_quatdiv; 
 

 
// Prototypes for methods: need a method for each incoming message 
void *quatdiv_new(long value); 	// object creation method  
void quatdiv_assist(t_quatdiv *quatdiv, void *b, long msg, long arg, char *s);
void quatdiv_free(t_quatdiv *quatdiv);
void quatdiv_list(t_quatdiv *x, Symbol *s, short argc, t_atom *argv);
void quatdiv_bang(t_quatdiv *x);
  
  
double quatleft[4], quatout[4];
double quatright[4], divider;



int main(void) 
{  
	// set up our class: create a class definition  
	setup((t_messlist**) &this_class, (method)quatdiv_new, (method)quatdiv_free, (short)sizeof(t_quatdiv), 0L,A_GIMME, 0); 

	addbang((method)quatdiv_bang);
    addmess((method)quatdiv_list, "list", A_GIMME, 0);
	addmess((method)quatdiv_assist, "assist", A_CANT, 0);
	finder_addclass("Maths","quatdiv");
	
	
	post("ahrs_quatdiv....loaded",0);

	return 0;
}


/*___________________------____________________________*/

/* ------------quatdiv_new --------------*/

void *quatdiv_new(long value) 
{ 
	t_quatdiv *quatdiv;

	quatdiv = (t_quatdiv *)newobject(this_class);	// create the new instance and return a pointer to it 
	
	quatdiv->m_quat = listout(quatdiv);
	
	quatdiv->m_proxy = proxy_new(quatdiv,1,&quatdiv->m_inletNumber);

	return(quatdiv);

} 





 
void quatdiv_list(t_quatdiv *x, Symbol *s, short argc, t_atom *argv)
{

	long i = proxy_getinlet((t_object *)x);

	if (argc > MAXSIZE){ 
	argc = MAXSIZE;
	post ("quatadd ERROR sorry, I need four numbers only (q0 q1 q2 q3 for each given quaternion).", 0);
	
	}
	else if (argc == MAXSIZE)
	{
			if (i == 0)
			{
		
					quatleft[0] =  argv[0].a_w.w_float;
					quatleft[1] =  argv[1].a_w.w_float;
					quatleft[2] =  argv[2].a_w.w_float;
					quatleft[3] =  argv[3].a_w.w_float;
			
			
					quatdiv_bang(x);
			
			}else{
					quatright[0] =  argv[0].a_w.w_float;
					quatright[1] =  argv[1].a_w.w_float;
					quatright[2] =  argv[2].a_w.w_float;
					quatright[3] =  argv[3].a_w.w_float;
								
			}

	
	}
	


	
}




void quatdiv_bang(t_quatdiv *x){


t_atom myList[3];


divider = 1.0 / pow(quatright[0], 2) + pow(quatright[1], 2) + pow(quatright[2], 2) + pow(quatright[3], 2);

						quatout[0] = (  (quatleft[0] * quatright[0]) + (quatleft[1] * quatright[1]) + (quatleft[2] * quatright[2]) + (quatleft[3] * quatright[3])  ) * divider ;
						quatout[1] = (  (quatleft[1] * quatright[0]) - (quatleft[0] * quatright[1]) - (quatleft[3] * quatright[2]) + (quatleft[2] * quatright[3])  ) * divider ;
						quatout[2] = (  (quatleft[2] * quatright[0]) + (quatleft[3] * quatright[1]) - (quatleft[0] * quatright[2]) - (quatleft[1] * quatright[3])  ) * divider ;
						quatout[3] = (  (quatleft[3] * quatright[0]) - (quatleft[2] * quatright[1]) + (quatleft[1] * quatright[2]) - (quatleft[0] * quatright[3])  ) * divider ;

short i;	
	
			for (i = 0 ; i< 4; i++){

						SETFLOAT(myList+i, quatout[i]);

			}


	outlet_list( x-> m_quat , 0L, 4, myList);



}




/* -------- quatdiv_assist ------------- */

void quatdiv_assist(t_quatdiv *quatdiv, void *b, long msg, long arg, char *s)
{
	if (msg == ASSIST_OUTLET) {
		switch (arg) {
			case 0: sprintf(s, "%s", "List with resulting quaternion");
				break;


		}
	}
	else if(msg == ASSIST_INLET)
				switch (arg) {
			case 0: sprintf(s, "First quaternion ");
				break;
			
			case 1: sprintf(s, "Second quaternion ");
				break;

		}
}
	

/* ----------- quatdiv_free -------------*/

void quatdiv_free(t_quatdiv *quatdiv)
{
		freeobject(quatdiv -> m_proxy);
		
		post("Fine!....BYE!", 0);

}


