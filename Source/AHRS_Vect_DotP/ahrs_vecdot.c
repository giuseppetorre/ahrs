/*
   
 "vecdot" Object for Max/Msp 4.6
 AHRS MAX Library
 Copyright (C) 2013  Giuseppe Torre
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 
 */


#include "ext.h"    
#include "ext_mess.h"
#define MAXSIZE 3

void *this_class; // Required. Global pointing to this class  

 
typedef struct _vecdot // Data structure for this object  
{ 
	t_object m_ob;	// Must always be the first field; used by Max  
	Atom m_args[3];
	void *m_proxy; /* 3 inlets requires 2 proxies */
	long m_inletNumber; /* where proxy will put inlet number */
	
	void *m_vec;


} t_vecdot; 
 

 
// Prototypes for methods: need a method for each incoming message 
void *vecdot_new(long value); 	// object creation method  
void vecdot_assist(t_vecdot *vecdot, void *b, long msg, long arg, char *s);
void vecdot_free(t_vecdot *vecdot);
void vecdot_list(t_vecdot *x, Symbol *s, short argc, t_atom *argv);
void vecdot_bang(t_vecdot *x);
  
  
double vecleft[3], dotproduct;
double vecright[3];



int main(void) 
{  
	// set up our class: create a class definition  
	setup((t_messlist**) &this_class, (method)vecdot_new, (method)vecdot_free, (short)sizeof(t_vecdot), 0L,A_GIMME, 0); 

	addbang((method)vecdot_bang);
    addmess((method)vecdot_list, "list", A_GIMME, 0);
	addmess((method)vecdot_assist, "assist", A_CANT, 0);
	finder_addclass("Maths","vecdot");
	
	
	post("ahrs_vecdot....loaded",0);

	return 0;
}


/*___________________------____________________________*/

/* ------------vecdot_new --------------*/

void *vecdot_new(long value) 
{ 
	t_vecdot *vecdot;

	vecdot = (t_vecdot *)newobject(this_class);	// create the new instance and return a pointer to it 
	
	vecdot->m_vec = listout(vecdot);
	
	vecdot->m_proxy = proxy_new(vecdot,1,&vecdot->m_inletNumber);

	return(vecdot);

} 





 
void vecdot_list(t_vecdot *x, Symbol *s, short argc, t_atom *argv)
{

	long i = proxy_getinlet((t_object *)x);

	if (argc > MAXSIZE){ 
	argc = MAXSIZE;
	post ("vecdot ERROR sorry, I need three numbers only (x y z for each given vector).", 0);
	
	}
	else if (argc == MAXSIZE)
	{
			if (i == 0)
			{
		
					vecleft[0] =  argv[0].a_w.w_float;
					vecleft[1] =  argv[1].a_w.w_float;
					vecleft[2] =  argv[2].a_w.w_float;
			
			
			
					vecdot_bang(x);
			
			}else{
					vecright[0] =  argv[0].a_w.w_float;
					vecright[1] =  argv[1].a_w.w_float;
					vecright[2] =  argv[2].a_w.w_float;
				
								
			}

	
	}
	


	
}




void vecdot_bang(t_vecdot *x){




dotproduct = (vecleft[0] * vecright[0]) + (vecleft[1] * vecright[1]) + (vecleft[2] * vecright[2]);


outlet_float( x-> m_vec , dotproduct);



}




/* -------- vecdot_assist ------------- */

void vecdot_assist(t_vecdot *vecdot, void *b, long msg, long arg, char *s)
{
	if (msg == ASSIST_OUTLET) {
		switch (arg) {
			case 0: sprintf(s, "%s", "List with resulting vector");
				break;


		}
	}
	else if(msg == ASSIST_INLET)
				switch (arg) {
			case 0: sprintf(s, "First vector ");
				break;
			
			case 1: sprintf(s, "Second vector ");
				break;

		}
}
	

/* ----------- vecdot_free -------------*/

void vecdot_free(t_vecdot *vecdot)
{
		freeobject(vecdot -> m_proxy);
		
		post("Fine!....BYE!", 0);

}


