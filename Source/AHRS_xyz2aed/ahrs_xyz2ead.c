/*
   
 "xyz2aed" Object for Max/Msp 4.6
 AHRS MAX Library
 Copyright (C) 2013  Giuseppe Torre
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 
 */


#include "ext.h"    
#include "ext_mess.h"
#define PI 3.1415926535897932384626433832795
#include <math.h>

void *this_class; // Required. Global pointing to this class  

 
typedef struct _xyz2aed // Data structure for this object  
{ 
	t_object m_ob;	// Must always be the first field; used by Max  
	Atom m_args[3];
	long m_value;	//inlet
	
	void *m_out;


} t_xyz2aed; 
 

 
// Prototypes for methods: need a method for each incoming message 
void *xyz2aed_new(long value); 	// object creation method  
void xyz2aed_assist(t_xyz2aed *xyz2aed, void *b, long msg, long arg, char *s);
void xyz2aed_free(t_xyz2aed *xyz2aed);
void xyz2aed_list(t_xyz2aed *x, Symbol *s, short argc, t_atom *argv);


  
  

  
double q0, q1, q2;
double xyzlist[3];
t_atom myList[3];



int main(void) 
{  
	// set up our class: create a class definition  
	setup((t_messlist**) &this_class, (method)xyz2aed_new, (method)xyz2aed_free, (short)sizeof(t_xyz2aed), 0L,A_GIMME, 0); 


    addmess((method)xyz2aed_list, "list", A_GIMME, 0);
	addmess((method)xyz2aed_assist, "assist", A_CANT, 0);
	finder_addclass("Maths","xyz2aed");
	
	


	post("ahrs_xyz2aed....loaded",0);

	return 0;
}


/*___________________------____________________________*/

/* ------------xyz2aed_new --------------*/

void *xyz2aed_new(long value) 
{ 
	t_xyz2aed *xyz2aed;

	xyz2aed = (t_xyz2aed *)newobject(this_class);	// create the new instance and return a pointer to it 
	
	xyz2aed->m_out = listout(xyz2aed);

	return(xyz2aed);

} 





 
void xyz2aed_list(t_xyz2aed *x, Symbol *s, short argc, t_atom *argv)
{

q0 = argv[0].a_w.w_float;
q1 = argv[1].a_w.w_float;
q2 = argv[2].a_w.w_float;


short i;

					
				
						xyzlist[0] = atan2(q1, q0) * 180.0 / PI;
						xyzlist[1] = atan2(  q2, sqrt( pow(q0,2) + pow(q1,2) ) ) * 180.0 / PI;
						xyzlist[2] = sqrt( pow(q0,2) + pow(q1,2) +  pow(q2,2)  )  ;

		


			for (i = 0 ; i< 3; i++){

						SETFLOAT(myList+i, xyzlist[i]);

			}


outlet_list( x-> m_out , 0L, 3, myList);

	
}


/* -------- xyz2aed_assist ------------- */

void xyz2aed_assist(t_xyz2aed *xyz2aed, void *b, long msg, long arg, char *s)
{
	if (msg == ASSIST_OUTLET) {
		switch (arg) {
			case 0: sprintf(s, "%s", "List with xyz ");
				break;


		}
	}
	else if(msg == ASSIST_INLET)
		sprintf(s, "%s", "aed coordinates");
}
	

/* ----------- xyz2aed_free -------------*/

void xyz2aed_free(t_xyz2aed *xyz2aed)
{

		post("Fine!....BYE!", 0);

}



