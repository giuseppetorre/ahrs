/*
 
 "quat2axis" Object for Max/Msp 4.6
 AHRS MAX Library
 Copyright (C) 2013  Giuseppe Torre
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 
 */


#include "ext.h"    
#include "ext_mess.h"
#include <string.h>

#include <stdio.h>
#include <math.h>



#define PI 3.1415926535897932384626433832795

 


void *this_class; // Required. Global pointing to this class  

 
typedef struct _quat2axis // Data structure for this object  
{ 
	t_object m_ob;	// Must always be the first field; used by Max  
	Atom m_args[4];
	long m_value;	//inlet
	
	void *m_quat;

	
} t_quat2axis; 
 

 
// Prototypes for methods: need a method for each incoming message 
void *quat2axis_new(long value); 	// object creation method  
void quat2axis_assist(t_quat2axis *quat2axis, void *b, long msg, long arg, char *s);
void quat2axis_free(t_quat2axis *quat2axis);
void quat2axis_list(t_quat2axis *x, Symbol *s, short argc, t_atom *argv);



double qw, qx, qy, qz;
double angleaxis[4];
double angle, ecs, y, z;
double norm; 
t_atom myList[4];
int i;



int main(void) 
{  
	// set up our class: create a class definition  
	setup((t_messlist**) &this_class, (method)quat2axis_new, (method)quat2axis_free, (short)sizeof(t_quat2axis), 0L,A_GIMME, 0); 


    addmess((method)quat2axis_list, "list", A_GIMME, 0);
	addmess((method)quat2axis_assist, "assist", A_CANT, 0);
	finder_addclass("Maths","quat2axis");
	
	


	post("...ahrs_quat2axis!...loaded!",0);

	return 0;
}


/*___________________------____________________________*/

/* ------------quat2axis_new --------------*/

void *quat2axis_new(long value) 
{ 
	t_quat2axis *quat2axis;

	quat2axis = (t_quat2axis *)newobject(this_class);	// create the new instance and return a pointer to it 
	
	
	quat2axis->m_quat = listout(quat2axis);

	return(quat2axis);

} 





 
void quat2axis_list(t_quat2axis *x, Symbol *s, short argc, t_atom *argv)
{




// http://www.euclideanspace.com/maths/geometry/rotations/conversions/quaternionToAngle/index.htm

 qw = argv[0].a_w.w_float;
 qx = argv[1].a_w.w_float;
 qy = argv[2].a_w.w_float;
 qz = argv[3].a_w.w_float;



  norm = sqrt( pow(qw,2) + pow(qx,2) + pow(qy,2) + pow(qz,2));


	if (qw == 1.0){
	
		angle = 0.0;
		ecs = 33.0;
		y = 33.0;
		z = 33.0;	
	
	}else if (qw == 0.0){
	
	
		angle = 180.0;
		ecs = qx;
		y = qy;
		z = qz;
		
	}else{
	
	angle = 2.0 * acos(qw) * 180.0 / PI;
	
    qx /= norm;
    qy /= norm;
    qz /= norm;
    qw /= norm;
	
	
	ecs = qx / sqrt( 1.0 - qw * qw);
	y   = qy / sqrt( 1.0 - qw * qw);
	z   = qz / sqrt( 1.0 - qw * qw);
	
	}

	angleaxis[0] = angle;
	angleaxis[1] = ecs;
	angleaxis[2] = y;
	angleaxis[3] = z;	
		
	for (i = 0 ; i< 4; i++)
	{
			SETFLOAT(myList+i, angleaxis[i]);
	}


	outlet_list( x-> m_quat , 0L, 4, myList);
	
	
}






/* -------- quat2axis_assist ------------- */

void quat2axis_assist(t_quat2axis *quat2axis, void *b, long msg, long arg, char *s)
{
	if (msg == ASSIST_OUTLET) {
		switch (arg) {
			case 0: sprintf(s, "%s", "angle axis format (list)");
				break;
	

		}
	}
	else if(msg == ASSIST_INLET)
		sprintf(s, "%s", "Quaternion (list)");
}
	

/* ----------- quat2axis_free -------------*/

void quat2axis_free(t_quat2axis *quat2axis)
{

		post("Fine!....BYE!", 0);

}



