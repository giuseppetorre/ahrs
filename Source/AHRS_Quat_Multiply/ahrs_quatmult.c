/*
   
 "quatmult" Object for Max/Msp 4.6
 AHRS MAX Library
 Copyright (C) 2013  Giuseppe Torre
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 
 */


#include "ext.h"    
#include "ext_mess.h"
#define MAXSIZE 4

void *this_class; // Required. Global pointing to this class  

 
typedef struct _quatmult // Data structure for this object  
{ 
	t_object m_ob;	// Must always be the first field; used by Max  
	Atom m_args[4];
	void *m_proxy; /* 3 inlets requires 2 proxies */
	long m_inletNumber; /* where proxy will put inlet number */
	
	void *m_quat;


} t_quatmult; 
 

 
// Prototypes for methods: need a method for each incoming message 
void *quatmult_new(long value); 	// object creation method  
void quatmult_assist(t_quatmult *quatmult, void *b, long msg, long arg, char *s);
void quatmult_free(t_quatmult *quatmult);
void quatmult_list(t_quatmult *x, Symbol *s, short argc, t_atom *argv);
void quatmult_bang(t_quatmult *x);
  
  
double quatleft[4], quatout[4];
double quatright[4];



int main(void) 
{  
	// set up our class: create a class definition  
	setup((t_messlist**) &this_class, (method)quatmult_new, (method)quatmult_free, (short)sizeof(t_quatmult), 0L,A_GIMME, 0); 

	addbang((method)quatmult_bang);
    addmess((method)quatmult_list, "list", A_GIMME, 0);
	addmess((method)quatmult_assist, "assist", A_CANT, 0);
	finder_addclass("Maths","quatmult");
	
	
	post("ahrs_quatmult....loaded",0);

	return 0;
}


/*___________________------____________________________*/

/* ------------quatmult_new --------------*/

void *quatmult_new(long value) 
{ 
	t_quatmult *quatmult;

	quatmult = (t_quatmult *)newobject(this_class);	// create the new instance and return a pointer to it 
	
	quatmult->m_quat = listout(quatmult);
	
	quatmult->m_proxy = proxy_new(quatmult,1,&quatmult->m_inletNumber);

	return(quatmult);

} 





 
void quatmult_list(t_quatmult *x, Symbol *s, short argc, t_atom *argv)
{

	long i = proxy_getinlet((t_object *)x);

	if (argc > MAXSIZE){ 
	argc = MAXSIZE;
	post ("quatadd ERROR sorry, I need four numbers only (q0 q1 q2 q3 for each given quaternion).", 0);
	
	}
	else if (argc == MAXSIZE)
	{
			if (i == 0)
			{
		
					quatleft[0] =  argv[0].a_w.w_float;
					quatleft[1] =  argv[1].a_w.w_float;
					quatleft[2] =  argv[2].a_w.w_float;
					quatleft[3] =  argv[3].a_w.w_float;
			
			
					quatmult_bang(x);
			
			}else{
					quatright[0] =  argv[0].a_w.w_float;
					quatright[1] =  argv[1].a_w.w_float;
					quatright[2] =  argv[2].a_w.w_float;
					quatright[3] =  argv[3].a_w.w_float;
								
			}

	
	}
	


	
}




void quatmult_bang(t_quatmult *x){


t_atom myList[3];


						quatout[0] = (quatleft[0] * quatright[0]) - (quatleft[1] * quatright[1]) - (quatleft[2] * quatright[2]) - (quatleft[3] * quatright[3]) ;
						quatout[1] = (quatleft[0] * quatright[1]) + (quatleft[1] * quatright[0]) + (quatleft[2] * quatright[3]) - (quatleft[3] * quatright[2]) ;
						quatout[2] = (quatleft[0] * quatright[2]) - (quatleft[1] * quatright[3]) + (quatleft[2] * quatright[0]) + (quatleft[3] * quatright[1]) ;
						quatout[3] = (quatleft[0] * quatright[3]) + (quatleft[1] * quatright[2]) - (quatleft[2] * quatright[1]) + (quatleft[3] * quatright[0]) ;

short i;	
	
			for (i = 0 ; i< 4; i++){

						SETFLOAT(myList+i, quatout[i]);

			}


	outlet_list( x-> m_quat , 0L, 4, myList);



}




/* -------- quatmult_assist ------------- */

void quatmult_assist(t_quatmult *quatmult, void *b, long msg, long arg, char *s)
{
	if (msg == ASSIST_OUTLET) {
		switch (arg) {
			case 0: sprintf(s, "%s", "List with resulting quaternion");
				break;


		}
	}
	else if(msg == ASSIST_INLET)
				switch (arg) {
			case 0: sprintf(s, "First quaternion ");
				break;
			
			case 1: sprintf(s, "Second quaternion ");
				break;

		}
}
	

/* ----------- quatmult_free -------------*/

void quatmult_free(t_quatmult *quatmult)
{
		freeobject(quatmult -> m_proxy);
		
		post("Fine!....BYE!", 0);

}


