/*
   
 "vecadd" Object for Max/Msp 4.6
 AHRS MAX Library
 Copyright (C) 2013  Giuseppe Torre
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 
 */


#include "ext.h"    
#include "ext_mess.h"
#define MAXSIZE 3

void *this_class; // Required. Global pointing to this class  

 
typedef struct _vecadd // Data structure for this object  
{ 
	t_object m_ob;	// Must always be the first field; used by Max  
	Atom m_args[3];
	void *m_proxy; /* 3 inlets requires 2 proxies */
	long m_inletNumber; /* where proxy will put inlet number */
	
	void *m_vec;


} t_vecadd; 
 

 
// Prototypes for methods: need a method for each incoming message 
void *vecadd_new(long value); 	// object creation method  
void vecadd_assist(t_vecadd *vecadd, void *b, long msg, long arg, char *s);
void vecadd_free(t_vecadd *vecadd);
void vecadd_list(t_vecadd *x, Symbol *s, short argc, t_atom *argv);
void vecadd_bang(t_vecadd *x);
  
  
double vecleft[3], vecout[3];
double vecright[3];



int main(void) 
{  
	// set up our class: create a class definition  
	setup((t_messlist**) &this_class, (method)vecadd_new, (method)vecadd_free, (short)sizeof(t_vecadd), 0L,A_GIMME, 0); 

	addbang((method)vecadd_bang);
    addmess((method)vecadd_list, "list", A_GIMME, 0);
	addmess((method)vecadd_assist, "assist", A_CANT, 0);
	finder_addclass("Maths","vecadd");
	
	
	post("ahrs_vecadd....loaded",0);

	return 0;
}


/*___________________------____________________________*/

/* ------------vecadd_new --------------*/

void *vecadd_new(long value) 
{ 
	t_vecadd *vecadd;

	vecadd = (t_vecadd *)newobject(this_class);	// create the new instance and return a pointer to it 
	
	vecadd->m_vec = listout(vecadd);
	
	vecadd->m_proxy = proxy_new(vecadd,1,&vecadd->m_inletNumber);

	return(vecadd);

} 





 
void vecadd_list(t_vecadd *x, Symbol *s, short argc, t_atom *argv)
{

	long i = proxy_getinlet((t_object *)x);

	if (argc > MAXSIZE){ 
	argc = MAXSIZE;
	post ("vecadd ERROR sorry, I need three numbers only (x y z for each given vector).", 0);
	
	}
	else if (argc == MAXSIZE)
	{
			if (i == 0)
			{
		
					vecleft[0] =  argv[0].a_w.w_float;
					vecleft[1] =  argv[1].a_w.w_float;
					vecleft[2] =  argv[2].a_w.w_float;
			
			
			
					vecadd_bang(x);
			
			}else{
					vecright[0] =  argv[0].a_w.w_float;
					vecright[1] =  argv[1].a_w.w_float;
					vecright[2] =  argv[2].a_w.w_float;
				
								
			}

	
	}
	


	
}




void vecadd_bang(t_vecadd *x){


t_atom myList[3];
	
						vecout[0] = vecleft[0] + vecright[0];
						vecout[1] = vecleft[1] + vecright[1];
						vecout[2] = vecleft[2] + vecright[2];
	
					

	
short i;	
	
			for (i = 0 ; i< 3; i++){

						SETFLOAT(myList+i, vecout[i]);

			}


	outlet_list( x-> m_vec , 0L, 4, myList);



}




/* -------- vecadd_assist ------------- */

void vecadd_assist(t_vecadd *vecadd, void *b, long msg, long arg, char *s)
{
	if (msg == ASSIST_OUTLET) {
		switch (arg) {
			case 0: sprintf(s, "%s", "List with resulting vector");
				break;


		}
	}
	else if(msg == ASSIST_INLET)
				switch (arg) {
			case 0: sprintf(s, "First vector ");
				break;
			
			case 1: sprintf(s, "Second vector ");
				break;

		}
}
	

/* ----------- vecadd_free -------------*/

void vecadd_free(t_vecadd *vecadd)
{
		freeobject(vecadd -> m_proxy);
		
		post("Fine!....BYE!", 0);

}


