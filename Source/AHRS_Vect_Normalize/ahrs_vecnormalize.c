/*
   
 "vecnormalize" Object for Max/Msp 4.6
 AHRS MAX Library
 Copyright (C) 2013  Giuseppe Torre
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 
 */


#include "ext.h"    
#include "ext_mess.h"


void *this_class; // Required. Global pointing to this class  

 
typedef struct _vecnormalize // Data structure for this object  
{ 
	t_object m_ob;	// Must always be the first field; used by Max  
	Atom m_args[3];
	long m_value;	//inlet
	
	void *m_vec;


} t_vecnormalize; 
 

 
// Prototypes for methods: need a method for each incoming message 
void *vecnormalize_new(long value); 	// object creation method  
void vecnormalize_assist(t_vecnormalize *vecnormalize, void *b, long msg, long arg, char *s);
void vecnormalize_free(t_vecnormalize *vecnormalize);
void vecnormalize_list(t_vecnormalize *x, Symbol *s, short argc, t_atom *argv);


  
  

  
double q0, q1, q2, magnitude;
double veclist[4];
t_atom myList[4];



int main(void) 
{  
	// set up our class: create a class definition  
	setup((t_messlist**) &this_class, (method)vecnormalize_new, (method)vecnormalize_free, (short)sizeof(t_vecnormalize), 0L,A_GIMME, 0); 


    addmess((method)vecnormalize_list, "list", A_GIMME, 0);
	addmess((method)vecnormalize_assist, "assist", A_CANT, 0);
	finder_addclass("Maths","vecnormalize");
	
	


	post("ahrs_vecnormalize....loaded",0);

	return 0;
}


/*___________________------____________________________*/

/* ------------vecnormalize_new --------------*/

void *vecnormalize_new(long value) 
{ 
	t_vecnormalize *vecnormalize;

	vecnormalize = (t_vecnormalize *)newobject(this_class);	// create the new instance and return a pointer to it 
	
	vecnormalize->m_vec = listout(vecnormalize);

	return(vecnormalize);

} 





 
void vecnormalize_list(t_vecnormalize *x, Symbol *s, short argc, t_atom *argv)
{

q0 = argv[0].a_w.w_float;
q1 = argv[1].a_w.w_float;
q2 = argv[2].a_w.w_float;

short i;
						
						magnitude = 1.0 /  sqrt( pow(q0,2) + pow(q1,2) + pow(q2,2));
				
						veclist[0] = q0 * magnitude;
						veclist[1] = q1 * magnitude;
						veclist[2] = q2 * magnitude;
				
		


			for (i = 0 ; i< 3; i++){

						SETFLOAT(myList+i, veclist[i]);

			}


outlet_list( x-> m_vec , 0L, 3, myList);

	
}


/* -------- vecnormalize_assist ------------- */

void vecnormalize_assist(t_vecnormalize *vecnormalize, void *b, long msg, long arg, char *s)
{
	if (msg == ASSIST_OUTLET) {
		switch (arg) {
			case 0: sprintf(s, "%s", "List with Normalized vector ");
				break;


		}
	}
	else if(msg == ASSIST_INLET)
		sprintf(s, "%s", "vector that needs to be normalized");
}
	

/* ----------- vecnormalize_free -------------*/

void vecnormalize_free(t_vecnormalize *vecnormalize)
{

		post("Fine!....BYE!", 0);

}



