/*
   
 "quatconjugate" Object for Max/Msp 4.6
 AHRS MAX Library
 Copyright (C) 2013  Giuseppe Torre
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 
 */


#include "ext.h"    
#include "ext_mess.h"


void *this_class; // Required. Global pointing to this class  

 
typedef struct _quatconjugate // Data structure for this object  
{ 
	t_object m_ob;	// Must always be the first field; used by Max  
	Atom m_args[4];
	long m_value;	//inlet
	
	void *m_quat;


} t_quatconjugate; 
 

 
// Prototypes for methods: need a method for each incoming message 
void *quatconjugate_new(long value); 	// object creation method  
void quatconjugate_assist(t_quatconjugate *quatconjugate, void *b, long msg, long arg, char *s);
void quatconjugate_free(t_quatconjugate *quatconjugate);
void quatconjugate_list(t_quatconjugate *x, Symbol *s, short argc, t_atom *argv);


  
  

  
double q0, q1, q2, q3, magnitude;
double quatlist[4];
t_atom myList[4];



int main(void) 
{  
	// set up our class: create a class definition  
	setup((t_messlist**) &this_class, (method)quatconjugate_new, (method)quatconjugate_free, (short)sizeof(t_quatconjugate), 0L,A_GIMME, 0); 


    addmess((method)quatconjugate_list, "list", A_GIMME, 0);
	addmess((method)quatconjugate_assist, "assist", A_CANT, 0);
	finder_addclass("Maths","quatconjugate");
	
	


	post("ahrs_quatconjugate....loaded",0);

	return 0;
}


/*___________________------____________________________*/

/* ------------quatconjugate_new --------------*/

void *quatconjugate_new(long value) 
{ 
	t_quatconjugate *quatconjugate;

	quatconjugate = (t_quatconjugate *)newobject(this_class);	// create the new instance and return a pointer to it 
	
	quatconjugate->m_quat = listout(quatconjugate);

	return(quatconjugate);

} 





 
void quatconjugate_list(t_quatconjugate *x, Symbol *s, short argc, t_atom *argv)
{

q0 = argv[0].a_w.w_float;
q1 = argv[1].a_w.w_float;
q2 = argv[2].a_w.w_float;
q3 = argv[3].a_w.w_float;
short i;
						
				
						quatlist[0] = q0 ;
						quatlist[1] = -1.0 * q1 ;
						quatlist[2] = -1.0 * q2 ;
						quatlist[3] = -1.0 * q3 ;
		


			for (i = 0 ; i< 4; i++){

						SETFLOAT(myList+i, quatlist[i]);

			}


outlet_list( x-> m_quat , 0L, 4, myList);

	
}


/* -------- quatconjugate_assist ------------- */

void quatconjugate_assist(t_quatconjugate *quatconjugate, void *b, long msg, long arg, char *s)
{
	if (msg == ASSIST_OUTLET) {
		switch (arg) {
			case 0: sprintf(s, "%s", "List with conjugate Quaternion ");
				break;


		}
	}
	else if(msg == ASSIST_INLET)
		sprintf(s, "%s", "quaternion that needs to be inverted");
}
	

/* ----------- quatconjugate_free -------------*/

void quatconjugate_free(t_quatconjugate *quatconjugate)
{

		post("Fine!....BYE!", 0);

}



