/*
   
 "vecmagn" Object for Max/Msp 4.6
 AHRS MAX Library
 Copyright (C) 2013  Giuseppe Torre
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 
 */


#include "ext.h"    
#include "ext_mess.h"


void *this_class; // Required. Global pointing to this class  

 
typedef struct _vecmagn // Data structure for this object  
{ 
	t_object m_ob;	// Must always be the first field; used by Max  
	Atom m_args[4];
	long m_value;	//inlet
	
	void *m_vec;


} t_vecmagn; 
 

 
// Prototypes for methods: need a method for each incoming message 
void *vecmagn_new(long value); 	// object creation method  
void vecmagn_assist(t_vecmagn *vecmagn, void *b, long msg, long arg, char *s);
void vecmagn_free(t_vecmagn *vecmagn);
void vecmagn_list(t_vecmagn *x, Symbol *s, short argc, t_atom *argv);


  
  

  
double q0, q1, q2, magnitude;
double veclist[3];
t_atom myList[3];



int main(void) 
{  
	// set up our class: create a class definition  
	setup((t_messlist**) &this_class, (method)vecmagn_new, (method)vecmagn_free, (short)sizeof(t_vecmagn), 0L,A_GIMME, 0); 


    addmess((method)vecmagn_list, "list", A_GIMME, 0);
	addmess((method)vecmagn_assist, "assist", A_CANT, 0);
	finder_addclass("Maths","vecmagn");
	
	


	post("ahrs_vecmagn....loaded",0);

	return 0;
}


/*___________________------____________________________*/

/* ------------vecmagn_new --------------*/

void *vecmagn_new(long value) 
{ 
	t_vecmagn *vecmagn;

	vecmagn = (t_vecmagn *)newobject(this_class);	// create the new instance and return a pointer to it 
	
	vecmagn->m_vec = listout(vecmagn);

	return(vecmagn);

} 





 
void vecmagn_list(t_vecmagn *x, Symbol *s, short argc, t_atom *argv)
{

q0 = argv[0].a_w.w_float;
q1 = argv[1].a_w.w_float;
q2 = argv[2].a_w.w_float;


						
						magnitude = sqrt( pow(q0,2) + pow(q1,2) + pow(q2,2)  );
				

		
outlet_float( x-> m_vec , magnitude);

	
}


/* -------- vecmagn_assist ------------- */

void vecmagn_assist(t_vecmagn *vecmagn, void *b, long msg, long arg, char *s)
{
	if (msg == ASSIST_OUTLET) {
		switch (arg) {
			case 0: sprintf(s, "%s", "Magnitude of vector ");
				break;


		}
	}
	else if(msg == ASSIST_INLET)
		sprintf(s, "%s", "vector ");
}
	

/* ----------- vecmagn_free -------------*/

void vecmagn_free(t_vecmagn *vecmagn)
{

		post("Fine!....BYE!", 0);

}



