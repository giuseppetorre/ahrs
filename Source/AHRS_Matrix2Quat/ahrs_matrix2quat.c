
 /*	
  
  "matrix2quat" Object for Max/Msp 4.6																							
  AHRS MAX Library
  Copyright (C) 2013  Giuseppe Torre
  
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  
 
  */


#include "ext.h"    
#include "ext_mess.h"
#include <string.h>
#include <stdio.h>
#include <math.h>
#define PI 3.1415926535897932384626433832795
#define EPSILON 1e-6


//******************CALIBRATION STUFF*************************//

/* INTRODUCE THE OFFSET VALUES IN THE FOLLOWING VARIABLES IN ADC values */

/*
#define GYROX_OFFSET 1905 //initial offset in accelerometer X
#define GYROY_OFFSET 1964 // initial offset in accelerometer Y
#define GYROZ_OFFSET 2053//initial offset in accelerometer Z

 WE SHOULD CALIBRATE THE FOLLOWING VALUES FOR EACH PARTICULAR IMU axis

#define ALPHA_RATE_RESOLUTION 0.01135
#define PHI_RATE_RESOLUTION   0.0128 
#define THETA_RATE_RESOLUTION 0.009   // volts/(grades/s)//   formula (5/4096) * (ADC Reading) / 200      ---- 200 is 33 rpm turntable / 60

*/


				//******************CALIBRATION STUFF*************************//


/* WE SHOULD CALIBRATE THE FOLOWING VALUES FOR EACH PARTICULAR IMU USING A TURNTABLE
   HOWEVER THE FOLLOWING VALUES SHOULD BE FINE TO START WITH,LEAVE THOSE VALUES AND WE'LL THINK ABOUT
   IT IN THE FUTURE*/	
/*
#define ALPHA_RATE_RESOLUTION 0.013234167
#define PHI_RATE_RESOLUTION   0.013234167 
#define THETA_RATE_RESOLUTION 0.013234167  // volts/(grades/s)//
*/
/* INTRODUCE THE SAMPLING RATE VALUE IN THE FOLLOWING VARIABLE.
NOTE: TO ESTIMATE THE TIME STEP, YOU CAN USE THE HYPERTERMINAL,START COMMUNICATION AND COUNT THE NUMBER OF DATA 
YOU GET IN 30 SECONDS (30 SECONDS FOR EXAMPLE),THEN: 

TIME STEP= 30 /(NUMBER OF DATA RECEIVED) */
//#define SAMPLING_RATE  0.1

 


void *this_class;				// Required. Global pointing to this class  

 
typedef struct _matrix2quat		// Data structure for this object  
{ 
	t_object m_ob;				// Must always be the first field; used by Max  
	
	Atom m_args[9];				// we want our inlet to be receiving a list of 10 elements
	
	long m_value;				//inlet
					
	void *m_R1;					//these is the outlet for the 3 X 3 Matrix
				// End Outlets ------------------------------------------------------------------
	
} t_matrix2quat; 
 

			// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
			// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
			
																											// - //  Prototypes for methods: need a method for each incoming message   // - //
																												//									----------MAX FUNCTIONS
			void *matrix2quat_new(long value);																	// object creation method  
			void matrix2quat_assist(t_matrix2quat *matrix2quat, void *b, long msg, long arg, char *s);			// thing to display help message when you move the mouse over an in/outlet
			void matrix2quat_free(t_matrix2quat *matrix2quat);													// free memory with the object instance
			
			void matrix2quat_list(t_matrix2quat *x, Symbol *s, short argc, t_atom *argv);						// what we going to do with our incoming list of values

			
			// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =

																											// - //

			double Quat[4], Matrix[9];
			double trace, Suca;
			

			t_atom myList[9];


int main(void) 
{  
	// set up our class: create a class definition  
	setup((t_messlist**) &this_class, (method)matrix2quat_new, (method)matrix2quat_free, (short)sizeof(t_matrix2quat), 0L,A_GIMME, 0); 


    addmess((method)matrix2quat_list, "list", A_GIMME, 0);
	addmess((method)matrix2quat_assist, "assist", A_CANT, 0);
	finder_addclass("Maths","matrix2quat");
	




	post("...ahrs_matrix2quat!...loaded",0);

	return 0;
}


/*___________________------____________________________*/

/* ------------matrix2quat_new --------------*/

void *matrix2quat_new(long value) 
{ 
	t_matrix2quat *matrix2quat;

	matrix2quat = (t_matrix2quat *)newobject(this_class);	// create the new instance and return a pointer to it 
	
    matrix2quat->m_R1 = listout(matrix2quat);


	return(matrix2quat);

} 





 
void matrix2quat_list(t_matrix2quat *x, Symbol *s, short argc, t_atom *argv)
{

int i;

// http://www.euclideanspace.com/maths/geometry/rotations/conversions/matrixToQuaternion/index.htm 

Matrix[0] = argv[0].a_w.w_float;
Matrix[1] = argv[1].a_w.w_float;
Matrix[2] = argv[2].a_w.w_float;
Matrix[3] = argv[3].a_w.w_float;
Matrix[4] = argv[4].a_w.w_float;
Matrix[5] = argv[5].a_w.w_float;
Matrix[6] = argv[6].a_w.w_float;
Matrix[7] = argv[7].a_w.w_float;
Matrix[8] = argv[8].a_w.w_float;


								trace = Matrix[0] + Matrix[4]  + Matrix[8] + 1.0;


								if( trace > EPSILON ){
      
										Suca = 0.5 / sqrt(trace);
										Quat[0] = 0.25 / Suca;											//w
										Quat[1] = (Matrix[7] - Matrix[5] ) * Suca; //x
										Quat[2] = (Matrix[2] - Matrix[6] ) * Suca; //y
										Quat[3] = (Matrix[3] - Matrix[1] ) * Suca; //z

	
								} else {

										if (    (  Matrix[0] > Matrix[4]   )    &&       (  Matrix[0]  > Matrix[8]   )     ){ 
				
												Suca = sqrt( 1.0 + Matrix[0]  - Matrix[4] - Matrix[8] ) * 2.0 ; // S=4*qx 
												Quat[0] = ( Matrix[7] - Matrix[5] ) / Suca;		// w	
												Quat[1] = 0.25 * Suca;										// x
												Quat[2] = ( Matrix[1] + Matrix[3] ) / Suca; // y 
												Quat[3] = ( Matrix[2] + Matrix[6] ) / Suca; // z 
										
				
										} else if (  Matrix[4] > Matrix[8] ) { 
		
												Suca = sqrt( 1.0 + Matrix[4] - Matrix[0] - Matrix[8] ) * 2.0; // S=4*qy
												Quat[0] = (Matrix[2] - Matrix[6]) / Suca;		// w
												Quat[1] = (Matrix[1] + Matrix[3] ) / Suca;  // x
												Quat[2] = 0.25 * Suca;									// y
												Quat[3] = (Matrix[5] + Matrix[7] ) / Suca;  // z 
												
										} else { 
		
												Suca = sqrt( 1.0 + Matrix[8] - Matrix[0] - Matrix[4] ) * 2.0; // S=4*qz
												Quat[0] = (Matrix[3] - Matrix[1] ) / Suca;  // w
												Quat[1] = (Matrix[2] + Matrix[6] ) / Suca;  // x
												Quat[2] = (Matrix[5] + Matrix[7] ) / Suca;  // y
												Quat[3] = 0.25 * Suca;									// z
										
										} 

								}


		

//Let's cook for output //							

	for (i = 0 ; i< 4; i++)
	{
		SETFLOAT(myList+i, Quat[i]);
	}


	outlet_list( x-> m_R1 , 0L, 4, myList);
	

}



/* -------- matrix2quat_assist ------------- */

void matrix2quat_assist(t_matrix2quat *matrix2quat, void *b, long msg, long arg, char *s)
{
	if (msg == ASSIST_OUTLET) {
		switch (arg) {
			case 0: sprintf(s, "%s", "quaternion (list) ");
				break;

		}
	}
	else if(msg == ASSIST_INLET)
		sprintf(s, "%s", "matrix 3x3 (list)");
}
	

/* ----------- matrix2quat_free -------------*/

void matrix2quat_free(t_matrix2quat *matrix2quat)
{

		post("Fine!....BYE!", 0);

}

//***********************************************************************************************************************************************//
//***********************************************************************************************************************************************//
//***********************************************************************************************************************************************//
//***********************************************************************************************************************************************//
//***********************************************************************************************************************************************//
//***********************************************************************************************************************************************//
//***********************************************************************************************************************************************//
//***********************************************************************************************************************************************//
//***********************************************************************************************************************************************//

