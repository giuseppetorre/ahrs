
 /*	
 																																														
 "quat2matrix" Object for Max/Msp 4.6																							
  AHRS MAX Library
  Copyright (C) 2013  Giuseppe Torre
  
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  
  */


#include "ext.h"    
#include "ext_mess.h"
#include <string.h>
#include <stdio.h>
#include <math.h>

 


void *this_class;				// Required. Global pointing to this class  

 
typedef struct _quat2matrix		// Data structure for this object  
{ 
	t_object m_ob;				// Must always be the first field; used by Max  
	
	Atom m_args[4];				// we want our inlet to be receiving a list of 10 elements
	
	long m_value;				//inlet
					
	void *m_R1;					//these is the outlet for the 3 X 3 Matrix
				// End Outlets ------------------------------------------------------------------
	
} t_quat2matrix; 
 

			// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
			// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
			
																											// - //  Prototypes for methods: need a method for each incoming message   // - //
																												//									----------MAX FUNCTIONS
			void *quat2matrix_new(long value);																	// object creation method  
			void quat2matrix_assist(t_quat2matrix *quat2matrix, void *b, long msg, long arg, char *s);			// thing to display help message when you move the mouse over an in/outlet
			void quat2matrix_free(t_quat2matrix *quat2matrix);													// free memory with the object instance
			
			void quat2matrix_list(t_quat2matrix *x, Symbol *s, short argc, t_atom *argv);						// what we going to do with our incoming list of values

			
			// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =

																											// - //

			double Quat[4], Matrix[9];			

			t_atom myList[9];


int main(void) 
{  
	// set up our class: create a class definition  
	setup((t_messlist**) &this_class, (method)quat2matrix_new, (method)quat2matrix_free, (short)sizeof(t_quat2matrix), 0L,A_GIMME, 0); 


    addmess((method)quat2matrix_list, "list", A_GIMME, 0);
	addmess((method)quat2matrix_assist, "assist", A_CANT, 0);
	finder_addclass("Maths","quat2matrix");
	




	post("...ahrs_quat2matrix!...loaded",0);

	return 0;
}


/*___________________------____________________________*/

/* ------------quat2matrix_new --------------*/

void *quat2matrix_new(long value) 
{ 
	t_quat2matrix *quat2matrix;

	quat2matrix = (t_quat2matrix *)newobject(this_class);	// create the new instance and return a pointer to it 
	
    quat2matrix->m_R1 = listout(quat2matrix);


	return(quat2matrix);

} 





 
void quat2matrix_list(t_quat2matrix *x, Symbol *s, short argc, t_atom *argv)
{

int i;

// http://www.euclideanspace.com/maths/geometry/rotations/conversions/quaternionToMatrix/index.htm 

Quat[0] = argv[0].a_w.w_float;
Quat[1] = argv[1].a_w.w_float;
Quat[2] = argv[2].a_w.w_float;
Quat[3] = argv[3].a_w.w_float;


						Matrix[0] = 1.0 - (   2.0 * pow(Quat[2], 2)  ) - (  2.0 * pow(Quat[3], 2)  );
						Matrix[1] = (   2.0 * Quat[1] * Quat[2]   ) - (   2.0 * Quat[3] * Quat[0]  );
						Matrix[2] = (   2.0 * Quat[1] * Quat[3]   ) + (   2.0 * Quat[2] * Quat[0]  );
						Matrix[3] = (   2.0 * Quat[1] * Quat[2]   ) + (   2.0 * Quat[3] * Quat[0]  );
						Matrix[4] = 1.0 - (  2.0 * pow(Quat[1], 2)   ) - (  2.0 * pow(Quat[3], 2)  );
						Matrix[5] = (   2.0 * Quat[2] * Quat[3]   ) - (   2.0 * Quat[1] * Quat[0]  );
						Matrix[6] = (   2.0 * Quat[1] * Quat[3]   ) - (   2.0 * Quat[2] * Quat[0]  );
						Matrix[7] = (   2.0 * Quat[2] * Quat[3]   ) + (   2.0 * Quat[1] * Quat[0]  );
						Matrix[8] = 1.0 - (  2.0 * pow(Quat[1], 2)   ) - (  2.0* pow(Quat[2], 2)   );
						


		

//Let's cook for output //							

	for (i = 0 ; i< 9; i++)
	{
		SETFLOAT(myList+i, Matrix[i]);
	}


	outlet_list( x-> m_R1 , 0L, 9, myList);
	

}



/* -------- quat2matrix_assist ------------- */

void quat2matrix_assist(t_quat2matrix *quat2matrix, void *b, long msg, long arg, char *s)
{
	if (msg == ASSIST_OUTLET) {
		switch (arg) {
			case 0: sprintf(s, "%s", "matrix 3x3 (list) ");
				break;

		}
	}
	else if(msg == ASSIST_INLET)
		sprintf(s, "%s", "quaternion (list)");
}
	

/* ----------- quat2matrix_free -------------*/

void quat2matrix_free(t_quat2matrix *quat2matrix)
{

		post("Fine!....BYE!", 0);

}

//***********************************************************************************************************************************************//
//***********************************************************************************************************************************************//
//***********************************************************************************************************************************************//
//***********************************************************************************************************************************************//
//***********************************************************************************************************************************************//
//***********************************************************************************************************************************************//
//***********************************************************************************************************************************************//
//***********************************************************************************************************************************************//
//***********************************************************************************************************************************************//

