/*
 
 "aed2xyz" Object for Max/Msp 4.6
 AHRS MAX Library
 Copyright (C) 2013  Giuseppe Torre
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 
 */


#include "ext.h"
#include "ext_mess.h"
#define PI 3.1415926535897932384626433832795
#include <math.h>

void *this_class; // Required. Global pointing to this class  

 
typedef struct _aed2xyz // Data structure for this object  
{ 
	t_object m_ob;	// Must always be the first field; used by Max  
	Atom m_args[3];
	long m_value;	//inlet
	
	void *m_out;


} t_aed2xyz; 
 

 
// Prototypes for methods: need a method for each incoming message 
void *aed2xyz_new(long value); 	// object creation method  
void aed2xyz_assist(t_aed2xyz *aed2xyz, void *b, long msg, long arg, char *s);
void aed2xyz_free(t_aed2xyz *aed2xyz);
void aed2xyz_list(t_aed2xyz *x, Symbol *s, short argc, t_atom *argv);


  
  

  
double q0, q1, q2;
double xyzlist[3];
t_atom myList[3];



int main(void) 
{  
	// set up our class: create a class definition  
	setup((t_messlist**) &this_class, (method)aed2xyz_new, (method)aed2xyz_free, (short)sizeof(t_aed2xyz), 0L,A_GIMME, 0); 


    addmess((method)aed2xyz_list, "list", A_GIMME, 0);
	addmess((method)aed2xyz_assist, "assist", A_CANT, 0);
	finder_addclass("Maths","aed2xyz");
	
	


	post("ahrs_aed2xyz....loaded",0);

	return 0;
}


/*___________________------____________________________*/

/* ------------aed2xyz_new --------------*/

void *aed2xyz_new(long value) 
{ 
	t_aed2xyz *aed2xyz;

	aed2xyz = (t_aed2xyz *)newobject(this_class);	// create the new instance and return a pointer to it 
	
	aed2xyz->m_out = listout(aed2xyz);

	return(aed2xyz);

} 





 
void aed2xyz_list(t_aed2xyz *x, Symbol *s, short argc, t_atom *argv)
{

q0 = (argv[0].a_w.w_float * PI) / 180.0;
q1 = (argv[1].a_w.w_float * PI) / 180.0;
q2 = (argv[2].a_w.w_float * PI) / 180.0;

short i;
						
				
						xyzlist[0] = cos(q1) * cos(q0);
						xyzlist[1] = cos(q1) * sin(q0);
						xyzlist[2] = sin(q1)  ;

		


			for (i = 0 ; i< 3; i++){

						SETFLOAT(myList+i, xyzlist[i]);

			}


outlet_list( x-> m_out , 0L, 3, myList);

	
}


/* -------- aed2xyz_assist ------------- */

void aed2xyz_assist(t_aed2xyz *aed2xyz, void *b, long msg, long arg, char *s)
{
	if (msg == ASSIST_OUTLET) {
		switch (arg) {
			case 0: sprintf(s, "%s", "List with aed ");
				break;


		}
	}
	else if(msg == ASSIST_INLET)
		sprintf(s, "%s", "list with xyz");
}
	

/* ----------- aed2xyz_free -------------*/

void aed2xyz_free(t_aed2xyz *aed2xyz)
{

		post("Fine!....BYE!", 0);

}



