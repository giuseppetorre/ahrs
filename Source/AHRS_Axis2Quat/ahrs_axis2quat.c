/*
   
 "axis2quat" Object for Max/Msp 4.6
 AHRS MAX Library
 Copyright (C) 2013  Giuseppe Torre
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 
 
 */


#include "ext.h"    
#include "ext_mess.h"
#include <string.h>

#include <stdio.h>
#include <math.h>



#define PI 3.1415926535897932384626433832795

 


void *this_class; // Required. Global pointing to this class  

 
typedef struct _axis2quat // Data structure for this object  
{ 
	t_object m_ob;	// Must always be the first field; used by Max  
	Atom m_args[4];
	long m_value;	//inlet
	
	void *m_quat;

	
} t_axis2quat; 
 

 
// Prototypes for methods: need a method for each incoming message 
void *axis2quat_new(long value); 	// object creation method  
void axis2quat_assist(t_axis2quat *axis2quat, void *b, long msg, long arg, char *s);
void axis2quat_free(t_axis2quat *axis2quat);
void axis2quat_list(t_axis2quat *x, Symbol *s, short argc, t_atom *argv);



double quat[4], esse, radians;
t_atom myList[4];
int i;



int main(void) 
{  
	// set up our class: create a class definition  
	setup((t_messlist**) &this_class, (method)axis2quat_new, (method)axis2quat_free, (short)sizeof(t_axis2quat), 0L,A_GIMME, 0); 


    addmess((method)axis2quat_list, "list", A_GIMME, 0);
	addmess((method)axis2quat_assist, "assist", A_CANT, 0);
	finder_addclass("Maths","axis2quat");
	
	


	post("...ahrs_axis2quat!...loaded!",0);

	return 0;
}


/*___________________------____________________________*/

/* ------------axis2quat_new --------------*/

void *axis2quat_new(long value) 
{ 
	t_axis2quat *axis2quat;

	axis2quat = (t_axis2quat *)newobject(this_class);	// create the new instance and return a pointer to it 
	
	
	axis2quat->m_quat = listout(axis2quat);

	return(axis2quat);

} 





 
void axis2quat_list(t_axis2quat *x, Symbol *s, short argc, t_atom *argv)
{




// http://www.euclideanspace.com/maths/geometry/rotations/conversions/angleToQuaternion/index.htm


 radians = argv[0].a_w.w_float * PI / 180.0;
 esse = sin( radians / 2.0);

 quat[0] = cos(radians / 2.0);
 quat[1] = argv[1].a_w.w_float * esse;
 quat[2] = argv[2].a_w.w_float * esse;
 quat[3] = argv[3].a_w.w_float * esse;



 		
	for (i = 0 ; i< 4; i++)
	{
			SETFLOAT(myList+i, quat[i]);
	}


	outlet_list( x-> m_quat , 0L, 4, myList);
	
	
}






/* -------- axis2quat_assist ------------- */

void axis2quat_assist(t_axis2quat *axis2quat, void *b, long msg, long arg, char *s)
{
	if (msg == ASSIST_OUTLET) {
		switch (arg) {
			case 0: sprintf(s, "%s", "quaternion format (list)");
				break;
	

		}
	}
	else if(msg == ASSIST_INLET)
		sprintf(s, "%s", "angle/axis (list)");
}
	

/* ----------- axis2quat_free -------------*/

void axis2quat_free(t_axis2quat *axis2quat)
{

		post("Fine!....BYE!", 0);

}



